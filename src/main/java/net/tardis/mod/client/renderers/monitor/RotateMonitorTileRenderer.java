package net.tardis.mod.client.renderers.monitor;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.RotateMonitorModel;
import net.tardis.mod.tileentities.monitors.RotateMonitorTile;

public class RotateMonitorTileRenderer extends TileEntityRenderer<RotateMonitorTile> {

	public RotateMonitorTileRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	public static final RotateMonitorModel MODEL = new RotateMonitorModel();

	@Override
	public void render(RotateMonitorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		matrixStackIn.push();
		matrixStackIn.translate(0.5, 1.5, 0.5);
		matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180));
		
		ResourceLocation texture = new ResourceLocation(Tardis.MODID, "textures/tiles/rotate_monitor.png");
		MODEL.render(matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(texture)), combinedLightIn, combinedOverlayIn, 1, 1, 1, 1.0F);



		matrixStackIn.pop();
	}

}
