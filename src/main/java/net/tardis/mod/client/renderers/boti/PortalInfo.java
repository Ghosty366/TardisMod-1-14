package net.tardis.mod.client.renderers.boti;

import java.util.UUID;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.IRenderTypeBuffer.Impl;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.misc.BiFunctionVoid;

public class PortalInfo {

	private UUID id;
	private Vector3d pos;
	private IVoidFunction<MatrixStack> translate;
	private IVoidFunction<MatrixStack> translatePortal;
	private BiFunctionVoid<MatrixStack, IRenderTypeBuffer.Impl> doorRenderer;
	private BiFunctionVoid<MatrixStack, IRenderTypeBuffer.Impl> portalRenderer;
	private WorldShell shell;
	
	public boolean canRenderFor(PlayerEntity player) {
		return true;
	}
	
	public UUID getId() {
		return this.id;
	}
	
	public void setId(UUID id) {
		this.id = id;
	}
	
	/** Translates this entire thing, runs after the translation in #setPosition() */
	public void setTranslate(IVoidFunction<MatrixStack> translate) {
		this.translate = translate;
	}
	
	/** Translates the world shell */
	public void setTranslatePortal(IVoidFunction<MatrixStack> translatePortal) {
		this.translatePortal = translatePortal;
	}
	
	public void translate(MatrixStack stack) {
		if(this.translate != null)
			this.translate.run(stack);
	}
	
	public void translatePortal(MatrixStack stack) {
		if(this.translatePortal != null)
			this.translatePortal.run(stack);
	}
	
	/** Base translation, make this your position in the world */
	public void setPosition(Vector3d pos) {
		this.pos = pos;
	}
	
	public void setPosition(BlockPos pos) {
		this.setPosition(new Vector3d(pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5));
	}
	
	public Vector3d getPosition() {
		return this.pos;
	}
	
	//World Shell
	public void setWorldShell(WorldShell shell) {
		this.shell = shell;
	}
	
	public WorldShell getWorldShell() {
		return this.shell;
	}
	
	public void renderPortal(MatrixStack stack, Impl imp) {
		if(this.portalRenderer != null)
			this.portalRenderer.run(stack, imp);
	}
	
	public void renderDoor(MatrixStack stack, Impl imp) {
		if(this.doorRenderer != null)
			this.doorRenderer.run(stack, imp);
	}
	
	/** Render inside the portal for doors that open into the portal */
	public void setRenderDoor(BiFunctionVoid<MatrixStack, IRenderTypeBuffer.Impl> door) {
		this.doorRenderer = door;
	}
	
	/** Render the portal cutout here */
	public void setRenderPortal(BiFunctionVoid<MatrixStack, IRenderTypeBuffer.Impl> portal) {
		this.portalRenderer = portal;
	}
	
	
	public static interface IVoidFunction<T>{
		void run(T instance);
	}
}
