package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.GlStateManager.DestFactor;
import com.mojang.blaze3d.platform.GlStateManager.SourceFactor;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.tardis.mod.tileentities.machines.ArtronCollectorTile;

public class ArtronCollectorRenderer extends TileEntityRenderer<ArtronCollectorTile> {

	public ArtronCollectorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void render(ArtronCollectorTile tile, float partialTicks, MatrixStack matrixStackIn,
	        IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		matrixStackIn.push();
		double flo = Math.cos(Minecraft.getInstance().world.getGameTime() * 0.05) * 0.05;
		matrixStackIn.translate(0.5, 1.5 + flo, 0.5);
		float scale = 0.6F;
		matrixStackIn.scale(scale, scale, scale);
		matrixStackIn.translate(0.5, 1.5 + flo, 0.5);
		Minecraft.getInstance().getItemRenderer().renderItem(tile.getItem(), TransformType.FIXED, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
		matrixStackIn.pop();
		
	}

}
