package net.tardis.mod.client.guis.containers;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.containers.AlembicContainer;
import net.tardis.mod.tileentities.AlembicTile;

public class AlembicScreen extends ContainerScreen<AlembicContainer> {

    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/containers/alembic.png");
    private final TranslationTextComponent title = new TranslationTextComponent("container.tardis.alembic");
    private AlembicTile tile;

    public AlembicScreen(AlembicContainer cont, PlayerInventory inv, ITextComponent titleIn) {
        super(cont, inv, titleIn);
        tile = cont.getAlembic();
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(MatrixStack matrixStack, float partialTicks, int x, int y) {
        this.renderBackground(matrixStack);
        this.minecraft.textureManager.bindTexture(TEXTURE);
        int texWidth = 176;
        int textHeight = 166;
        this.blit(matrixStack, width / 2 - texWidth / 2, height / 2 - textHeight / 2, 0, 0, texWidth, textHeight);

        //Water
        int waterScaled = (int) (53 * ((tile.getWaterTank().getFluidAmount() / tile.getWaterTank().getCapacity())));
        int offsetWaterBlit = (53 - waterScaled);
        blit(matrixStack, width / 2 - 42, height / 2 - 73 + offsetWaterBlit, 193, 19 + offsetWaterBlit, 10, 53 - offsetWaterBlit);
        
        //Mercury
        int mercuryScaled = (int) (53 * ((tile.getMercury() / 1000.0)));
        int offsetMercuryBlit = (53 - mercuryScaled);
        blit(matrixStack, width / 2 + 31, height / 2 - 73 + offsetMercuryBlit, 207, 19 + offsetMercuryBlit, 10, 53 - offsetMercuryBlit);

        //Fire
        if (tile.getMaxBurnTime() > 0) {
            int fireScaled = (int) (13 * (1 - (tile.getBurnTime() / (double) tile.getMaxBurnTime())));
            blit(matrixStack, width / 2 - 25, height / 2 - 53 + fireScaled, 203, 1 + fireScaled, 13, 13 - fireScaled);
        }
        
        //Progress Bar
        blit(matrixStack, width / 2 + 2, height / 2 - 52, 178, 1, (int) (23 * tile.getPercent()), 18);
        
        //Water level lines
        blit(matrixStack, width / 2 - 39, height / 2 - 71, 182, 21, 7, 49);
        
        //Mercury level lines
        blit(matrixStack, width / 2 + 34, height / 2 - 71, 182, 21, 7, 49);
    }
    
    

    @Override
    protected void drawGuiContainerForegroundLayer(MatrixStack matrixStack, int x, int y) {
        this.font.drawText(matrixStack, this.playerInventory.getDisplayName(), (float)this.playerInventoryTitleX, (float)this.playerInventoryTitleY, 4210752);
        
        
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderHoveredTooltip(matrixStack, mouseX, mouseY);
        drawCenteredString(matrixStack, this.minecraft.fontRenderer, title.getString(), this.width / 2, this.guiTop - 13, 0xFFFFFF);
    }
}
