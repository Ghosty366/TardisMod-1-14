package net.tardis.mod.client.models.entity.dalek;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;

public interface IDalekModel {
    void setDalek(DalekEntity dalek);

    DalekEntity getDalekEntity();

    ModelRenderer getLaser();
}