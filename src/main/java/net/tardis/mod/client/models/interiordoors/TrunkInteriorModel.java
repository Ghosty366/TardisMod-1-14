package net.tardis.mod.client.models.interiordoors;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.renderers.DoorRenderer;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.exteriors.TrunkExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.ConsoleTile;

public class TrunkInteriorModel extends EntityModel<Entity> implements IInteriorDoorRenderer{
    private final ModelRenderer boti;
    private final ModelRenderer lid_rotate_y;
    private final ModelRenderer walls2;
    private final ModelRenderer back_wall2;
    private final ModelRenderer sidewall_right2;
    private final ModelRenderer sidewall_left2;
    private final ModelRenderer top_wall2;
    private final ModelRenderer bottom_wall2;
    private final ModelRenderer trim2;
    private final ModelRenderer corners2;
    private final ModelRenderer knobs9;
    private final ModelRenderer knobs10;
    private final ModelRenderer knobs11;
    private final ModelRenderer knobs12;
    private final ModelRenderer knobs13;
    private final ModelRenderer knobs14;
    private final ModelRenderer knobs15;
    private final ModelRenderer knobs16;
    private final ModelRenderer mount2;
    private final ModelRenderer latch2;
    private final ModelRenderer sticker3;
    private final ModelRenderer sticker4;
    private final ModelRenderer frame;
    private final ModelRenderer trim3;
    private final ModelRenderer knobs2;
    private final ModelRenderer knobs3;
    private final ModelRenderer knobs4;
    private final ModelRenderer knobs5;

    public TrunkInteriorModel() {
        textureWidth = 256;
        textureHeight = 256;

        boti = new ModelRenderer(this);
        boti.setRotationPoint(0.0F, 24.0F, -5.8125F);
        boti.setTextureOffset(4, 181).addBox(-15.0F, -64.0F, -0.25F, 30.0F, 62.0F, 2.0F, 0.0F, false);

        lid_rotate_y = new ModelRenderer(this);
        lid_rotate_y.setRotationPoint(17.0F, 24.0F, -4.5F);
        

        walls2 = new ModelRenderer(this);
        walls2.setRotationPoint(-17.0F, 0.0F, 4.5F);
        lid_rotate_y.addChild(walls2);
        

        back_wall2 = new ModelRenderer(this);
        back_wall2.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls2.addChild(back_wall2);
        back_wall2.setTextureOffset(123, 80).addBox(-16.0F, -65.0F, -15.0F, 32.0F, 64.0F, 1.0F, 0.0F, false);
        back_wall2.setTextureOffset(1, 2).addBox(-15.0F, -64.0F, -7.0F, 30.0F, 62.0F, 1.0F, 0.0F, false);

        sidewall_right2 = new ModelRenderer(this);
        sidewall_right2.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls2.addChild(sidewall_right2);
        sidewall_right2.setTextureOffset(124, 78).addBox(15.0F, -64.0F, -14.0F, 1.0F, 62.0F, 9.0F, 0.0F, false);

        sidewall_left2 = new ModelRenderer(this);
        sidewall_left2.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls2.addChild(sidewall_left2);
        sidewall_left2.setTextureOffset(128, 76).addBox(-16.0F, -64.0F, -14.0F, 1.0F, 62.0F, 9.0F, 0.0F, false);

        top_wall2 = new ModelRenderer(this);
        top_wall2.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls2.addChild(top_wall2);
        top_wall2.setTextureOffset(120, 117).addBox(-16.0F, -65.0F, -14.0F, 32.0F, 1.0F, 9.0F, 0.0F, false);

        bottom_wall2 = new ModelRenderer(this);
        bottom_wall2.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls2.addChild(bottom_wall2);
        bottom_wall2.setTextureOffset(152, 117).addBox(-16.0F, -2.0F, -14.0F, 32.0F, 1.0F, 9.0F, 0.0F, false);

        trim2 = new ModelRenderer(this);
        trim2.setRotationPoint(-17.0F, 0.0F, 4.5F);
        lid_rotate_y.addChild(trim2);
        trim2.setTextureOffset(77, 3).addBox(-16.5F, -62.0F, -6.5F, 2.0F, 58.0F, 2.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(14.5F, -62.0F, -6.5F, 2.0F, 58.0F, 2.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(-15.0F, -65.5F, -15.5F, 30.0F, 2.0F, 2.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(-15.0F, -2.5F, -15.5F, 30.0F, 2.0F, 2.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(14.5F, -2.5F, -12.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(-16.5F, -2.5F, -12.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(-16.5F, -65.5F, -12.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(14.5F, -65.5F, -12.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(14.5F, -64.0F, -15.5F, 2.0F, 62.0F, 2.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(-16.5F, -64.0F, -15.5F, 2.0F, 62.0F, 2.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(-13.0F, -2.5F, -6.5F, 26.0F, 2.0F, 2.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(-13.0F, -65.5F, -6.5F, 26.0F, 2.0F, 2.0F, 0.0F, false);

        corners2 = new ModelRenderer(this);
        corners2.setRotationPoint(-17.0F, 0.0F, 4.5F);
        lid_rotate_y.addChild(corners2);
        

        knobs9 = new ModelRenderer(this);
        knobs9.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs9);
        knobs9.setTextureOffset(74, 71).addBox(-17.0F, -4.0F, -16.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs9.setTextureOffset(74, 71).addBox(-16.0F, -3.0F, -16.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs9.setTextureOffset(74, 71).addBox(-17.5F, -3.0F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs9.setTextureOffset(74, 71).addBox(-16.0F, -1.5F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs10 = new ModelRenderer(this);
        knobs10.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs10);
        knobs10.setTextureOffset(74, 71).addBox(13.0F, -4.0F, -16.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs10.setTextureOffset(74, 71).addBox(14.0F, -3.0F, -16.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs10.setTextureOffset(74, 71).addBox(15.5F, -3.0F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs10.setTextureOffset(74, 71).addBox(14.0F, -1.5F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs11 = new ModelRenderer(this);
        knobs11.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs11);
        knobs11.setTextureOffset(74, 71).addBox(13.0F, -66.0F, -16.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs11.setTextureOffset(74, 71).addBox(14.0F, -65.0F, -16.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs11.setTextureOffset(74, 71).addBox(15.5F, -65.0F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs11.setTextureOffset(74, 71).addBox(14.0F, -66.5F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs12 = new ModelRenderer(this);
        knobs12.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs12);
        knobs12.setTextureOffset(74, 71).addBox(-17.0F, -66.0F, -16.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs12.setTextureOffset(74, 71).addBox(-16.0F, -65.0F, -16.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs12.setTextureOffset(74, 71).addBox(-17.5F, -65.0F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs12.setTextureOffset(74, 71).addBox(-16.0F, -66.5F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs13 = new ModelRenderer(this);
        knobs13.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs13);
        knobs13.setTextureOffset(74, 71).addBox(-17.0F, -66.0F, -8.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs13.setTextureOffset(74, 71).addBox(-17.0F, -66.0F, -8.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs13.setTextureOffset(74, 71).addBox(-17.5F, -65.0F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs13.setTextureOffset(74, 71).addBox(-17.5F, -65.0F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs13.setTextureOffset(74, 71).addBox(-16.0F, -66.5F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs13.setTextureOffset(74, 71).addBox(-16.0F, -66.5F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs14 = new ModelRenderer(this);
        knobs14.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs14);
        knobs14.setTextureOffset(74, 71).addBox(13.0F, -66.0F, -8.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs14.setTextureOffset(74, 71).addBox(13.0F, -66.0F, -8.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs14.setTextureOffset(74, 71).addBox(15.5F, -65.0F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs14.setTextureOffset(74, 71).addBox(15.5F, -65.0F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs14.setTextureOffset(74, 71).addBox(14.0F, -66.5F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs14.setTextureOffset(74, 71).addBox(14.0F, -66.5F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs15 = new ModelRenderer(this);
        knobs15.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs15);
        knobs15.setTextureOffset(74, 71).addBox(13.0F, -4.0F, -8.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs15.setTextureOffset(74, 71).addBox(13.0F, -4.0F, -8.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs15.setTextureOffset(74, 71).addBox(15.5F, -3.0F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs15.setTextureOffset(74, 71).addBox(15.5F, -3.0F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs15.setTextureOffset(74, 71).addBox(14.0F, -1.5F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs15.setTextureOffset(74, 71).addBox(14.0F, -1.5F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs16 = new ModelRenderer(this);
        knobs16.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs16);
        knobs16.setTextureOffset(74, 71).addBox(-17.0F, -4.0F, -8.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs16.setTextureOffset(74, 71).addBox(-17.0F, -4.0F, -8.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs16.setTextureOffset(74, 71).addBox(-17.5F, -3.0F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs16.setTextureOffset(74, 71).addBox(-17.5F, -3.0F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs16.setTextureOffset(74, 71).addBox(-16.0F, -1.5F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs16.setTextureOffset(74, 71).addBox(-16.0F, -1.5F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        mount2 = new ModelRenderer(this);
        mount2.setRotationPoint(-17.0F, 0.0F, 4.5F);
        lid_rotate_y.addChild(mount2);
        mount2.setTextureOffset(61, 73).addBox(16.0F, -12.0F, -9.0F, 1.0F, 2.0F, 4.0F, 0.0F, false);
        mount2.setTextureOffset(61, 73).addBox(16.0F, -36.0F, -9.0F, 1.0F, 2.0F, 4.0F, 0.0F, false);
        mount2.setTextureOffset(61, 73).addBox(16.0F, -56.0F, -9.0F, 1.0F, 2.0F, 4.0F, 0.0F, false);

        latch2 = new ModelRenderer(this);
        latch2.setRotationPoint(-17.0F, 0.0F, 4.5F);
        lid_rotate_y.addChild(latch2);
        latch2.setTextureOffset(51, 76).addBox(-17.0F, -36.0F, -9.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);

        sticker3 = new ModelRenderer(this);
        sticker3.setRotationPoint(-20.0F, -45.0F, -11.0F);
        lid_rotate_y.addChild(sticker3);
        setRotationAngle(sticker3, 0.0F, 0.0F, -0.6109F);
        sticker3.setTextureOffset(3, 158).addBox(-7.0F, -3.0F, 0.4F, 14.0F, 6.0F, 1.0F, 0.0F, false);

        sticker4 = new ModelRenderer(this);
        sticker4.setRotationPoint(0.0F, 0.0F, -0.5F);
        sticker3.addChild(sticker4);
        setRotationAngle(sticker4, 0.0F, 0.0F, 0.7854F);
        sticker4.setTextureOffset(10, 154).addBox(-4.0F, -4.0F, 0.8F, 8.0F, 8.0F, 1.0F, 0.0F, false);

        frame = new ModelRenderer(this);
        frame.setRotationPoint(17.0F, 24.0F, -1.9688F);
        

        trim3 = new ModelRenderer(this);
        trim3.setRotationPoint(-17.0F, 0.0F, 2.5F);
        frame.addChild(trim3);
        trim3.setTextureOffset(77, 3).addBox(-16.5F, -62.0F, -5.0F, 2.0F, 58.0F, 1.0F, 0.0F, false);
        trim3.setTextureOffset(77, 3).addBox(14.5F, -62.0F, -5.0F, 2.0F, 58.0F, 1.0F, 0.0F, false);
        trim3.setTextureOffset(77, 3).addBox(-13.0F, -2.5F, -5.0F, 26.0F, 2.0F, 1.0F, 0.0F, false);
        trim3.setTextureOffset(77, 3).addBox(-13.0F, -65.5F, -5.0F, 26.0F, 2.0F, 1.0F, 0.0F, false);

        knobs2 = new ModelRenderer(this);
        knobs2.setRotationPoint(-17.0F, 0.0F, 4.5F);
        frame.addChild(knobs2);
        knobs2.setTextureOffset(74, 71).addBox(-17.0F, -4.0F, -7.0F, 4.0F, 4.0F, 2.0F, 0.0F, false);

        knobs3 = new ModelRenderer(this);
        knobs3.setRotationPoint(-17.0F, 0.0F, 4.5F);
        frame.addChild(knobs3);
        knobs3.setTextureOffset(74, 71).addBox(13.0F, -4.0F, -7.0F, 4.0F, 4.0F, 2.0F, 0.0F, false);

        knobs4 = new ModelRenderer(this);
        knobs4.setRotationPoint(-17.0F, 0.0F, 4.5F);
        frame.addChild(knobs4);
        knobs4.setTextureOffset(74, 71).addBox(13.0F, -66.0F, -7.0F, 4.0F, 4.0F, 2.0F, 0.0F, false);

        knobs5 = new ModelRenderer(this);
        knobs5.setRotationPoint(-17.0F, 0.0F, 4.5F);
        frame.addChild(knobs5);
        knobs5.setTextureOffset(74, 71).addBox(-17.0F, -66.0F, -7.0F, 4.0F, 4.0F, 2.0F, 0.0F, false);
    }

    @Override
    public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
        //previously the render function, render code was moved to a method below
    }

    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void render(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {
    	matrixStack.push();
    	matrixStack.translate(0, 0.7, -0.45);
    	
    	matrixStack.scale(0.5F, 0.5F, 0.5F);
    	this.lid_rotate_y.rotateAngleY = (float) Math.toRadians(EnumDoorType.TRUNK.getRotationForState(door.getOpenState()));
    	
        //lid_rotate_y.render(matrixStack, buffer, packedLight, packedOverlay);
        frame.render(matrixStack, buffer, packedLight, packedOverlay);
        matrixStack.pop();
        
         if(Minecraft.getInstance().world != null) {
         	Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
         		
         		PortalInfo info = new PortalInfo();
         		info.setPosition(door.getPositionVec());
         		info.setWorldShell(data.getBotiWorld());
         		info.setTranslate(matrix -> {
         			DoorRenderer.applyTranslations(matrix, door.rotationYaw - 180, door.getHorizontalFacing());
         			matrix.rotate(Vector3f.ZN.rotationDegrees(180));
         			matrix.translate(0, -0.15, -0.45);
         		});
         		info.setTranslatePortal(matrix -> {
         			matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(data.getBotiWorld().getPortalDirection())));
         			matrix.translate(-0.5, -1.25, -0.5);
         		});
         		
         		info.setRenderPortal((matrix, buf) -> {
         			matrix.push();
         			matrix.scale(0.5F, 0.5F, 0.5F);
         			this.boti.render(matrix, buf.getBuffer(RenderType.getEntityCutout(getTexture())), packedLight, packedOverlay);
         			matrix.pop();
         		});
         		
         		info.setRenderDoor((matrix, buf) -> {
         			matrix.push();
         			matrix.rotate(Vector3f.ZN.rotationDegrees(180));
         			matrix.translate(0, 0.5, 0);
         			matrix.scale(0.5F, 0.5F, 0.5F);
         			this.lid_rotate_y.render(matrix, buf.getBuffer(RenderType.getEntityCutout(getTexture())), packedLight, packedOverlay);
         			matrix.pop();
         		});
         		
         		
         		BOTIRenderer.addPortal(info);
         	});
         }
        
    }

    @Override
    public ResourceLocation getTexture() {
        ConsoleTile tile = TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).orElse(null);
        if (tile != null) {
          int index = tile.getExteriorManager().getExteriorVariant();
          if (tile.getExterior().getVariants() != null && index < tile.getExterior().getVariants().length)
              return tile.getExterior().getVariants()[index].getTexture();
        }
        return TrunkExteriorRenderer.TEXTURE;
    }
}