package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.tileentities.ConsoleTile;

public abstract class TCommand implements Command<CommandSource> {

	public static final TranslationTextComponent MUST_BE_IN_TARDIS = new TranslationTextComponent("command.tardis.error.must_be_in_tardis");
	
    private int permLevel = 2;

    public TCommand() {}
    
    public TCommand(int level) {
        this.permLevel = level;
    }

    public boolean canExecute(CommandSource source) throws CommandSyntaxException {
        return source.hasPermissionLevel(permLevel);
    }
}
