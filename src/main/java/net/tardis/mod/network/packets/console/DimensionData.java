package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.controls.DimensionControl;
import net.tardis.mod.tileentities.ConsoleTile;

public class DimensionData implements ConsoleData{

	private int dimAmounts = 0;
	private int dimIndex = 0;
	
	public DimensionData(int dim, int index) {
		this.dimAmounts = dim;
		this.dimIndex = index;
	}
	
	@Override
	public void applyToConsole(ConsoleTile tile, Supplier<Context> context) {
		tile.getControl(DimensionControl.class).ifPresent(control -> {
			control.setAvaliableDimensions(dimAmounts);
			control.setDimIndex(this.dimIndex);
		});
	}

	@Override
	public void serialize(PacketBuffer buff) {
		buff.writeInt(dimAmounts);
		buff.writeInt(this.dimIndex);
	}

	@Override
	public void deserialize(PacketBuffer buff) {
		this.dimAmounts = buff.readInt();
		this.dimIndex = buff.readInt();
	}

}
