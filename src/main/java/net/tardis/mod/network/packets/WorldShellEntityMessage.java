package net.tardis.mod.network.packets;

import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.boti.stores.EntityStorage;
import net.tardis.mod.client.ClientPacketHandler;
import net.tardis.mod.constants.Constants;

public class WorldShellEntityMessage {

	List<EntityStorage> entities = Lists.newArrayList();
	BlockPos pos;
	int type = 0;
	
	public WorldShellEntityMessage(BlockPos pos, List<EntityStorage> entity) {
		
		this.pos = pos;
		type = 0;
		
		this.entities = Lists.newArrayList(entities);
	}
	
	public WorldShellEntityMessage(List<EntityStorage> entities) {
		type = 1;
		this.entities = Lists.newArrayList(entities);
	}
	

	public static void encode(WorldShellEntityMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.type);
		if(mes.type == 0)
			buf.writeBlockPos(mes.pos);
		
		buf.writeInt(mes.entities.size());
		for(EntityStorage store : mes.entities) {
			store.encode(buf);
		}
		
	}
	
	public static WorldShellEntityMessage decode(PacketBuffer buf) {
		BlockPos pos = null;
		List<EntityStorage> list = Lists.newArrayList();
		int size = buf.readInt();
		int type = buf.readInt();
        if (buf.readableBytes() <= Constants.PACKET_BYTE_LIMIT && buf.readerIndex() <= buf.writerIndex() - 1) {
        	if(type == 0)
    			pos = buf.readBlockPos();
    		for(int i = 0; i < size; ++i) {
    			list.add(new EntityStorage(buf));
    		}
    		if(pos == null || type == 1)
    			return new WorldShellEntityMessage(list);
		}
		
		return new WorldShellEntityMessage(pos, list);
	}
	
	
	public static void handle(WorldShellEntityMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			ClientPacketHandler.handleWorldShellEntity(mes.type, mes.entities, mes.pos);
		});
		context.get().setPacketHandled(true);
	}
}
