package net.tardis.mod.items;

import com.google.common.collect.Lists;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants.NBT;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.IMultiblock;
import net.tardis.mod.tileentities.WaypointBankTile;

import java.util.Iterator;
import java.util.List;

public class DataCrystalItem extends Item {

    public static final String WAYPOINT_KEY = "waypoints";
    public static final int MAX_WAYPOINTS = 5;

    public DataCrystalItem(Properties properties) {
        super(properties);
    }


    //Item Stuff

    public static List<SpaceTimeCoord> getStoredWaypoints(ItemStack stack) {

        if (!stack.hasTag() || !stack.getTag().contains(WAYPOINT_KEY))
            return Lists.newArrayList();

        List<SpaceTimeCoord> coords = Lists.newArrayList();
        ListNBT list = stack.getOrCreateTag().getList(WAYPOINT_KEY, NBT.TAG_COMPOUND);

        for (INBT nbt : list) {
            coords.add(SpaceTimeCoord.deserialize((CompoundNBT) nbt));
        }

        return coords;
    }

    public static void setStoredWaypoints(ItemStack stack, List<SpaceTimeCoord> coords) {

        ListNBT list = new ListNBT();

        for (SpaceTimeCoord coord : coords)
            list.add(coord.serialize());

        stack.getOrCreateTag().put(WAYPOINT_KEY, list);

    }


    //Data stuff

    public static boolean addWaypoint(ItemStack stack, SpaceTimeCoord coord) {
        List<SpaceTimeCoord> coords = getStoredWaypoints(stack);

        if (coords.size() >= MAX_WAYPOINTS)
            return false;

        coords.add(coord);
        setStoredWaypoints(stack, coords);
        return true;

    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        TileEntity te = context.getWorld().getTileEntity(context.getPos());

        WaypointBankTile bank = null;
        if (te instanceof WaypointBankTile)
            bank = (WaypointBankTile) te;
        else if (te instanceof IMultiblock) {
            if (((IMultiblock) te).getMasterTile() instanceof WaypointBankTile)
                bank = (WaypointBankTile) ((IMultiblock) te).getMasterTile();
        }
        if (bank == null)
            return ActionResultType.PASS;

        //If bank is not null
        if (!context.getWorld().isRemote) {
            List<SpaceTimeCoord> preSaved = getStoredWaypoints(context.getItem());
            if (!preSaved.isEmpty()) {
                Iterator<SpaceTimeCoord> it = preSaved.iterator();
                while (it.hasNext()) {
                    SpaceTimeCoord coord = it.next();
                    if (bank.addWaypoint(coord))
                        it.remove();
                }
                setStoredWaypoints(context.getItem(), preSaved);
                PlayerHelper.sendMessageToPlayer(context.getPlayer(), new TranslationTextComponent("message.data_crystal.transfer_to_bank"), true);
            } else {
                List<SpaceTimeCoord> coords = Lists.newArrayList(bank.getWaypoints());
                coords.removeIf(coord -> coord.equals(SpaceTimeCoord.UNIVERAL_CENTER));
                setStoredWaypoints(context.getItem(), coords);
                bank.clearWaypoints();
                PlayerHelper.sendMessageToPlayer(context.getPlayer(), new TranslationTextComponent("message.data_crystal.extract_from_bank"), true);
            }
        }

        return ActionResultType.SUCCESS;
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        List<SpaceTimeCoord> coords = getStoredWaypoints(stack);
        if (!coords.isEmpty()) {
            for (SpaceTimeCoord coord : coords) {
                tooltip.add(new TranslationTextComponent("tooltip.tardis.data_crystal", coord.getName()));
            }
        } else {
            tooltip.add(new TranslationTextComponent("tooltip.tardis.data_crystal.empty"));
        }
        tooltip.add(Constants.Translations.TOOLTIP_HOLD_SHIFT);
        if (Screen.hasShiftDown()) {
            tooltip.clear();
            tooltip.add(0, stack.getDisplayName());
            tooltip.add(new TranslationTextComponent("tooltip.tardis.data_crystal_use"));
        }
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}
