package net.tardis.mod.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.misc.ConsoleBoundWithTooltipItem;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.upgrades.KeyFobUpgrade;

public class KeyItem extends ConsoleBoundWithTooltipItem{

    public KeyItem() {
        super(new Properties().maxStackSize(1).group(TItemGroups.MAIN));
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        ItemStack stack = playerIn.getHeldItem(handIn);
        if (stack.getItem() instanceof KeyItem) {
            if (!worldIn.isRemote) {
                TardisHelper.getConsole(worldIn.getServer(), getTardis(stack)).ifPresent(tile -> {
                    tile.getUpgrade(KeyFobUpgrade.class).ifPresent(fob -> {
                        if (fob.isUsable()) {
                            //Open near the exterior
                            if (tile.getCurrentLocation().withinDistance(playerIn.getPosition(), 16) && tile.getCurrentDimension() == playerIn.world.getDimensionKey()) {
                                ExteriorTile ext = tile.getExterior().getExterior(tile);
                                if (ext != null) {
                                    ext.setLocked(!ext.getLocked());
                                    if (ext.getLocked()) {
                                        ext.setDoorState(EnumDoorState.CLOSED);
                                    }
                                    ext.setOther();
                                    worldIn.playSound(null, playerIn.getPosition(), TSounds.CAR_LOCK.get(), SoundCategory.BLOCKS, 1F, 1F);
                                    playerIn.sendStatusMessage(ext.getLocked() ? ExteriorBlock.LOCKED : ExteriorBlock.UNLOCKED, true);
                                }
                            }
                            //Open in the TARDIS dimension
                            else if (playerIn.world == tile.getWorld()) {
                                tile.getDoor().ifPresent(door -> {
                                    door.setLocked(!door.isLocked());
                                    if (door.isLocked())
                                        door.setOpenState(EnumDoorState.CLOSED);
                                    door.updateOther();
                                    worldIn.playSound(null, playerIn.getPosition(), TSounds.CAR_LOCK.get(), SoundCategory.BLOCKS, 1F, 1F);
                                });
                            }
                        }
                    });
                });
                if (getTardis(stack) == null) {
                	PlayerHelper.sendMessageToPlayer(playerIn, Constants.Translations.ITEM_NOT_ATTUNED, true);
                }
            }
            return ActionResult.resultSuccess(stack);
        }
        return super.onItemRightClick(worldIn, playerIn, handIn);
    }
}
