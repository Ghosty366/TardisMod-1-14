package net.tardis.mod.tileentities.console.misc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.tileentities.ConsoleTile;

public class MonitorOverride implements INBTSerializable<CompoundNBT>{

	List<String> text = new ArrayList<>();
	long timeToDisplay;
	
	public MonitorOverride(ConsoleTile tile, int timeToDisplay, String... text) {
		this.timeToDisplay = timeToDisplay + tile.getWorld().getGameTime();
		this.text = Arrays.asList(text);
	}
	
	public MonitorOverride(ConsoleTile tile, int timeToDisplay, List<String> text) {
		this.timeToDisplay = timeToDisplay + tile.getWorld().getGameTime();
		this.text = text;
	}
	
	public MonitorOverride(CompoundNBT compound) {
		this.deserializeNBT(compound);
	}

	public boolean shouldRemove(ConsoleTile tile) {
		return tile.getWorld().getGameTime() >= this.timeToDisplay;
	}
	
	public String[] getText() {
		String[] textArray = new String[this.text.size()];
		this.text.toArray(textArray); //fills the array
		return textArray;
	}
	
	public List<String> getTextList(){
		return this.text;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putLong("time", this.timeToDisplay);
		
		ListNBT text = new ListNBT();
		for(String s : this.text) {
			text.add(StringNBT.valueOf(s));
		}
		
		tag.put("text", text);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.timeToDisplay = tag.getLong("time");
		ListNBT list = tag.getList("text", NBT.TAG_STRING);
		list.forEach(nbt -> {
			this.text.add(nbt.getString());
		});
	}
}
