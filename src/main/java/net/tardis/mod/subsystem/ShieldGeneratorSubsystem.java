package net.tardis.mod.subsystem;

import net.minecraft.item.Item;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.SparkingLevel;
/** Protects other subsystems from explosion damage to the exterior*/
public class ShieldGeneratorSubsystem extends Subsystem{
	
	public ShieldGeneratorSubsystem(ConsoleTile console, Item item) {
		super(console, item);
	}

	@Override
	public void onTakeoff() {}

	@Override
	public void onLand() {}

	@Override
	public void onFlightSecond() {}

	@Override
	public boolean stopsFlight() {
		return false;
	}

	@Override
	public SparkingLevel getSparkState() {
		return SparkingLevel.NONE;
	}
}
