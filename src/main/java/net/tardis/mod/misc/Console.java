package net.tardis.mod.misc;

import java.util.function.Supplier;

import net.minecraft.block.BlockState;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.Tardis;

public class Console extends ForgeRegistryEntry<Console>{
	
	private String translationKey;
	private ResourceLocation imageLocation;
	private Supplier<BlockState> state;

	public Console(Supplier<BlockState> state, ResourceLocation imageLocation) {
		this.state = state;
		this.imageLocation = imageLocation;
	}
	
	public Console(Supplier<BlockState> state, String imageLocation) {
		this(state, new ResourceLocation(Tardis.MODID, "textures/gui/consoles/" + imageLocation + ".png"));
	}
	
	public BlockState getState() {
		return state.get();
	}
	
	public ResourceLocation getPreviewTexture() {
		return this.imageLocation;
	}
	
	public String getTranslationKey() {
		if (this.translationKey == null) {
			this.translationKey = Util.makeTranslationKey("console", this.getRegistryName());
		}
		return this.translationKey;
	}
	
	public TranslationTextComponent getDisplayName() {
		return new TranslationTextComponent(this.getTranslationKey());
	}

}
