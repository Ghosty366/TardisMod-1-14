package net.tardis.mod.misc;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.Logger;

import com.google.gson.JsonElement;
import com.mojang.serialization.Codec;
import com.mojang.serialization.JsonOps;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.ars.ARSPiece;

public class ARSPieceCodecListener extends CodecJsonDataListener<ARSPiece> {

	public ARSPieceCodecListener(String folderName, Codec<ARSPiece> codec, Logger logger) {
		super(folderName, codec, logger);
	}

	@Override
	public Map<ResourceLocation, ARSPiece> mapValues(Map<ResourceLocation, JsonElement> inputs) {
		Map<ResourceLocation, ARSPiece> map = ARSPiece.registerCoreARSPieces();
		Map<ResourceLocation, ARSPiece> parsedInData = new HashMap<>();
		for (Entry<ResourceLocation, JsonElement> entry : inputs.entrySet()){
			ResourceLocation key = entry.getKey();
			JsonElement element = entry.getValue();
			// if we fail to parse json, log an error and continue
			// if we succeeded, add the resulting ARSPiece to the map
			this.codec.decode(JsonOps.INSTANCE, element)
				.get()
				.ifLeft(result -> {result.getFirst().setRegistryName(key); parsedInData.put(key, result.getFirst()); this.logger.info("Added Datapack entry: {}", key.toString());})
				.ifRight(partial -> this.logger.error("Failed to parse data json for {} due to: {}", key.toString(), partial.message()));
		}
		map.putAll(parsedInData);
		return map;
	}

	@Override
	public void setData(Map<ResourceLocation, ARSPiece> input) {
		for (Entry<ResourceLocation, ARSPiece> entry : input.entrySet()) {
			entry.getValue().setRegistryName(entry.getKey());
		}
		super.setData(input);
	}
	
	

}
