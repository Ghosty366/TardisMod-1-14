package net.tardis.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.machines.ArtronCollectorTile;

public class ArtronCollectorBlock extends TileBlock {

	public ArtronCollectorBlock() {
		super(Prop.Blocks.BASIC_TECH.get());
	}

	@Override
	public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		
		if(handIn == Hand.OFF_HAND)
			return ActionResultType.FAIL;
		
		ArtronCollectorTile tile = (ArtronCollectorTile)worldIn.getTileEntity(pos);
		ItemStack stack = player.getHeldItem(handIn);
			
		if(!tile.getItem().isEmpty())
			this.dropCurrentItem(tile, player);
		
		tile.placeItem(stack.copy());
		player.getHeldItem(handIn).shrink(1);
		tile.update();
		return ActionResultType.SUCCESS;
	}
	
	@Override
	public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
		if(state.getBlock() != newState.getBlock()) {
			ArtronCollectorTile collect = (ArtronCollectorTile)worldIn.getTileEntity(pos);
			if(!collect.getItem().isEmpty())
				InventoryHelper.spawnItemStack(worldIn, pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5, collect.getItem());
		}
		super.onReplaced(state, worldIn, pos, newState, isMoving);
	}

	/**
	 * 
	 * @param tile
	 * @return - True if anything was spawned
	 */
	public boolean dropCurrentItem(ArtronCollectorTile tile, PlayerEntity player) {
		if(!tile.getItem().isEmpty()) {
			TInventoryHelper.giveStackTo(player, tile.getItem());
			tile.placeItem(ItemStack.EMPTY);
			tile.update();
			return true;
		}
		return false;
	}

}
