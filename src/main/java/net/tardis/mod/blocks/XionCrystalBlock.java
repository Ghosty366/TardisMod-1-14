package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.tardis.mod.properties.Prop;

import javax.annotation.Nullable;

public class XionCrystalBlock extends Block {

    public static final IntegerProperty TYPE = IntegerProperty.create("type", 0, 5);

    public XionCrystalBlock(){
        super(Prop.Blocks.BASIC_CRYSTAL.get().notSolid().variableOpacity().setLightLevel(state -> 15));
        this.setDefaultState(this.getStateContainer().getBaseState().with(BlockStateProperties.WATERLOGGED, false));
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return super.getStateForPlacement(context);
    }

    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        super.onReplaced(state, worldIn, pos, newState, isMoving);

        if(state.getBlock() != newState.getBlock()){
            if(newState.getBlock() == TBlocks.xion_crystal.get()){
                worldIn.setBlockState(pos, state.with(TYPE, worldIn.rand.nextInt(6)));
            }
        }


    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        super.fillStateContainer(builder.add(TYPE, BlockStateProperties.WATERLOGGED));
    }
}
