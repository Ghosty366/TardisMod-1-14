
package net.tardis.mod.exterior;

import java.util.function.Supplier;

import net.minecraft.block.BlockState;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.misc.IDoorSoundScheme;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.upgrades.AtriumUpgrade;

public class TwoBlockBasicExterior extends AbstractExterior{

	private Supplier<BlockState> top;
	private TexVariant[] variants = null;
	
	public TwoBlockBasicExterior(Supplier<BlockState> state, boolean isChameleon, IDoorType type, IDoorSoundScheme sounds, ResourceLocation blueprint) {
		super(isChameleon, type, sounds, blueprint);
		this.top = state;
	}
	
	public TwoBlockBasicExterior(Supplier<BlockState> state, boolean inChameleon, IDoorType type,  IDoorSoundScheme sounds, ResourceLocation blueprint, TexVariant[] variants) {
		this(state, inChameleon, type, sounds, blueprint);
		this.variants = variants;
	}

	@Override
	public void demat(ConsoleTile console) {
		ServerWorld world = console.getWorld().getServer().getWorld(console.getCurrentDimension());
		if(world != null) {
			if(this.getExterior(console) != null)
				this.getExterior(console).demat();
		}
		else System.err.println("Tryed to demat, but TARDIS location dimension is null!");
	}

	@Override
	public void remat(ConsoleTile console) {
		ServerWorld world = console.getWorld().getServer().getWorld(console.getDestinationDimension());
		if(world != null) {
			BlockPos pos = console.getDestinationPosition().up();
			boolean isWater = world.getFluidState(pos).isTagged(FluidTags.WATER);
			BlockState state = top.get().with(BlockStateProperties.HORIZONTAL_FACING, console.getExteriorFacingDirection());
			if(state.hasProperty(BlockStateProperties.WATERLOGGED))
				state = state.with(BlockStateProperties.WATERLOGGED, isWater);
			world.setBlockState(pos, state);
			world.setBlockState(pos.down(), TBlocks.bottom_exterior.get().getDefaultState().with(BlockStateProperties.WATERLOGGED, world.getFluidState(pos.down()).getFluidState().isTagged(FluidTags.WATER)));
			
			TileEntity ext = world.getTileEntity(pos);
			if(ext instanceof ExteriorTile) {
				ExteriorTile exter = (ExteriorTile)ext;
				exter.copyConsoleData(console);
				exter.remat();
			}
		}
		else System.err.println("Tried to Remat, but TARDIS destination dimension is null!");
	}

	@Override
	public boolean shouldAddToChameleon() {
		return this.inChameleon;
	}

	@Override
	public int getWidth(ConsoleTile console) {
		AtriumUpgrade atrium = console.getUpgrade(AtriumUpgrade.class).orElse(null);
		if(atrium != null) {
			if(atrium.isActive())
				return atrium.getWidth();
		}
		return 0;
	}

	@Override
	public int getHeight(ConsoleTile console) {
		AtriumUpgrade atrium = console.getUpgrade(AtriumUpgrade.class).orElse(null);
		if(atrium != null) {
			if(atrium.isActive())
				return atrium.getHeight();
		}
		return 2;
	}

	@Override
	public ExteriorTile getExterior(ConsoleTile console) {
		if(!console.getWorld().isRemote) {
			ServerWorld world = console.getWorld().getServer().getWorld(console.getCurrentDimension());
			if(world != null) {
				TileEntity te = world.getTileEntity(console.getCurrentLocation().up());
				if(te instanceof ExteriorTile)
					return (ExteriorTile)te;
			}
		}
		return null;
	}

	@Override
	public ResourceLocation getBlueprintTexture() {
		return this.blueprint;
	}

	@Override
	public IDoorType getDoorType() {
		return this.type;
	}

	@Override
	public void remove(ConsoleTile tile) {
		ServerWorld world = tile.getWorld().getServer().getWorld(tile.getCurrentDimension());
		if(world != null) {
		    if (this.getExterior(tile) != null)
		        this.getExterior(tile).deleteExteriorBlocks();
		    else {
		        System.err.println("Exterior was not found!");
		    }
		}
		else System.err.println("World was not loadable!");
	}

	@Override
	public void place(ConsoleTile tile, RegistryKey<World> world, BlockPos pos) {
		ServerWorld targetWorld = tile.getWorld().getServer().getWorld(world);
		if(targetWorld != null) {
			
			boolean isInWater = targetWorld.getFluidState(pos.up()).isTagged(FluidTags.WATER);
			BlockState state = this.top.get().with(BlockStateProperties.HORIZONTAL_FACING, tile.getExteriorFacingDirection());
			if(state.hasProperty(BlockStateProperties.WATERLOGGED))
				state = state.with(BlockStateProperties.WATERLOGGED, isInWater);
			targetWorld.setBlockState(pos.up(), state);
			targetWorld.setBlockState(pos, TBlocks.bottom_exterior.get().getDefaultState().with(BlockStateProperties.WATERLOGGED, targetWorld.getFluidState(pos).getFluidState().isTagged(FluidTags.WATER)));
			
			tile.setCurrentLocation(targetWorld.getDimensionKey(), pos);
			if(targetWorld.getTileEntity(pos.up()) instanceof ExteriorTile) {
				ExteriorTile exterior= ((ExteriorTile)targetWorld.getTileEntity(pos.up()));
				exterior.copyConsoleData(tile);
				exterior.placeExteriorBlocks();
			}
		}
		else System.err.println("World was not loadable!");
	}

	@Override
	public TranslationTextComponent getDisplayName() {
		return new TranslationTextComponent("exterior.tardis." + this.getRegistryName().getPath());
	}

	@Override
	public TexVariant[] getVariants() {
		return this.variants;
	}

	@Override
	public BlockState getDefaultState() {
		return top.get();
	}

	@Override
	public IDoorSoundScheme getDoorSounds() {
		return this.sounds;
	}
}
