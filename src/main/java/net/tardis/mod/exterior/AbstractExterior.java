package net.tardis.mod.exterior;

import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.misc.IDoorSoundScheme;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public abstract class AbstractExterior extends ForgeRegistryEntry<AbstractExterior>{
	
	protected boolean inChameleon = false;
	protected ResourceLocation blueprint = null;
	protected IDoorType type;
	protected TexVariant[] variants = null;
	protected IDoorSoundScheme sounds;
	protected String translationKey;
	
	public AbstractExterior(boolean inChameleon, IDoorType type, IDoorSoundScheme sounds, ResourceLocation blueprint) {
		this.inChameleon = inChameleon;
		this.blueprint = blueprint;
		this.type = type;
		this.sounds = sounds;
	}
	
	public abstract void demat(ConsoleTile console);
	
	public abstract void remat(ConsoleTile console);
	
	public abstract ExteriorTile getExterior(ConsoleTile console);
	
	public abstract void remove(ConsoleTile tile);
	public abstract void place(ConsoleTile tile, RegistryKey<World> dimension, BlockPos pos);
	
	/**
	 * Whether this is added to the chameleon circuit by default
	 */
	public boolean shouldAddToChameleon() {return inChameleon;}
	public abstract BlockState getDefaultState();
	
	public abstract int getWidth(ConsoleTile console);
	public abstract int getHeight(ConsoleTile console);
	
	public ResourceLocation getBlueprintTexture() {return blueprint;}
	
	public IDoorType getDoorType() {return type;}
	
	public String getTranslationKey() {
		if (this.translationKey == null) {
			this.translationKey = Util.makeTranslationKey("exterior", this.getRegistryName());
		}
		return this.translationKey;
	}
	
	public TranslationTextComponent getDisplayName() {
		return new TranslationTextComponent(this.getTranslationKey());
	}
	
	@Nullable
	public TexVariant[] getVariants() {return variants;}
	
	public IDoorSoundScheme getDoorSounds() {return sounds;}
}
