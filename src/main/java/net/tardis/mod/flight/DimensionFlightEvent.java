package net.tardis.mod.flight;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class DimensionFlightEvent extends FlightEvent{
	
	public DimensionFlightEvent(FlightEventFactory entry, ArrayList<ResourceLocation> controls) {
		super(entry, controls);
	}

	@Override
	public void onMiss(ConsoleTile tile) {
		super.onMiss(tile);
		
		List<World> worlds = WorldHelper.getAllValidDimensions();
		tile.setDestination(worlds.get(ConsoleTile.rand.nextInt(worlds.size())).getDimensionKey(), tile.getDestinationPosition());
		
	}

}
