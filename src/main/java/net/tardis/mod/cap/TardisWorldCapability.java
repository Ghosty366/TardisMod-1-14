package net.tardis.mod.cap;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.IntNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.artron.IArtronBattery;
import net.tardis.mod.boti.BotiHandler;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.energy.TardisEnergy;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.misc.AttunableItem;
import net.tardis.mod.items.misc.IAttunable;
import net.tardis.mod.misc.AttunementHandler;
import net.tardis.mod.misc.TardisNames;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.AttunementProgressMessage;
import net.tardis.mod.network.packets.BOTIMessage;
import net.tardis.mod.recipe.AttunableRecipe;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.inventory.PanelInventory;
import net.tardis.mod.tileentities.inventory.PanelInventoryWrapper;

public class TardisWorldCapability implements ITardisWorldData {

    public static final int SHELL_RADIUS = 20;
    public static final AxisAlignedBB BOTI_BOX = AxisAlignedBB.withSizeAtOrigin(SHELL_RADIUS, SHELL_RADIUS, SHELL_RADIUS);
    public PanelInventory northInv;
    public PanelInventory eastInv;
    public PanelInventory southInv;
    public PanelInventory westInv;
    private World world;
    private TardisEnergy power;
    private ItemStackHandler itemBuffer;
    private String name;
    private BotiHandler botiHandler;
    private AttunementHandler attunementHandler;

    private WorldShell shell;

    public TardisWorldCapability(World world) {
        this.northInv = new PanelInventory(Direction.NORTH, 8);
        this.eastInv = new PanelInventory(Direction.EAST, 5);
        this.southInv = new PanelInventory(Direction.SOUTH, 10);
        this.westInv = new PanelInventory(Direction.WEST, 10);
        this.power = new TardisEnergy(1024);
        this.itemBuffer = new ItemStackHandler(9);
        this.world = world;
        this.botiHandler = new BotiHandler(this, SHELL_RADIUS);
        this.attunementHandler = new AttunementHandler(() -> eastInv, this.world);

    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.put("north_inv", northInv.serializeNBT());
        tag.put("east_inv", eastInv.serializeNBT());
        tag.put("south_inv", southInv.serializeNBT());
        tag.put("west_inv", westInv.serializeNBT());
        tag.put("power", this.power.serialize());
        tag.put("item_buffer", this.itemBuffer.serializeNBT());
        if(this.name != null)
        	tag.putString("name", this.name);
        this.attunementHandler.serialize(tag);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT tag) {
        northInv.deserializeNBT(tag.getList("north_inv", Constants.NBT.TAG_COMPOUND));
        eastInv.deserializeNBT(tag.getList("east_inv", Constants.NBT.TAG_COMPOUND));
        southInv.deserializeNBT(tag.getList("south_inv", Constants.NBT.TAG_COMPOUND));
        westInv.deserializeNBT(tag.getList("west_inv", Constants.NBT.TAG_COMPOUND));
        power.deserialize((IntNBT) tag.get("power"));
        this.itemBuffer.deserializeNBT(tag.getCompound("item_buffer"));
        if(tag.contains("name"))
        	this.name = tag.getString("name");
        else this.name = TardisNames.getRandomName(world.rand);
        this.attunementHandler.deserialize(tag);
    }

    @Override
    public PanelInventory getEngineInventoryForSide(Direction dir) {
        switch (dir) {
            case EAST:
                return eastInv;
            case SOUTH:
                return southInv;
            case WEST:
                return westInv;
            default:
                return northInv;
        }
    }

    @Override
    public void tick() {

        //Artron charging
        if (!world.isRemote && world.getGameTime() % 200 == 0) {
            for (int index = 0; index < eastInv.getHandler().getSlots(); ++index) {
                ItemStack slotStack = eastInv.getStackInSlot(index);
                if (slotStack.getItem() instanceof IArtronBattery) {
                    ((IArtronBattery) slotStack.getItem())
                            .charge(slotStack, 10);
                }
            }
        }
        
        if (!world.isRemote) {
            if (world.getGameTime() % 20 == 0)
                this.buildBoti();

            TardisHelper.getConsoleInWorld(world).ifPresent(console -> {
        		if(console.isInFlight())
        		    this.attunementHandler.tick(console);
           		    //this.attunementTick();
            });
        }

        if(this.shell != null)
            this.shell.tick(world.isRemote);

        
    }

    @Override
    public TardisEnergy getEnergy() {
        return this.power;
    }

    @Override
    public ItemStackHandler getItemBuffer() {
        return this.itemBuffer;
    }

    @Override
    public AttunementHandler getAttunementHandler() {
        return this.attunementHandler;
    }

    @Override
    public WorldShell getBotiWorld() {
        return this.shell;
    }

    @Override
    public void setBotiWorld(WorldShell shell) {
        this.shell = shell;
    }

    @Override
    public BOTIMessage createMessage(WorldShell shell) {
        return new BOTIMessage(shell);
    }

    public void buildBoti() {
    	
        TardisHelper.getConsoleInWorld(world).ifPresent(tile -> {
        	
        	ServerWorld world = this.world.getServer().getWorld(tile.getCurrentDimension());

        	this.botiHandler.updateBoti(world, tile.getCurrentLocation(), BlockPos.ZERO.offset(tile.getTrueExteriorFacingDirection(), (int)Math.ceil(SHELL_RADIUS / 2.0)));

        	//Set portal direction
            this.getBotiWorld().setPortalDirection(tile.getTrueExteriorFacingDirection());

            Network.sendToAllInWorld(new BOTIMessage(shell), (ServerWorld) this.world);

        });

    }

	@Override
	public String getTARDISName() {

        if(this.name == null || this.name.isEmpty())
            this.name = TardisNames.getRandomName(world.rand);

		return name;
	}

}
