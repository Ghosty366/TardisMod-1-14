package net.tardis.mod.traits;

import net.tardis.mod.tileentities.ConsoleTile;

public class AgoraphobicTrait extends TardisTrait{

	public AgoraphobicTrait(TardisTraitType type) {
		super(type);
		
	}

	@Override
	public void tick(ConsoleTile tile) {}

}
