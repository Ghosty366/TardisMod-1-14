package net.tardis.mod.registries;

import java.util.function.Supplier;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.schematics.ExteriorUnlockSchematic;
import net.tardis.mod.schematics.InteriorUnlockSchematic;
import net.tardis.mod.schematics.Schematic;

public class SchematicRegistry {
	
    public static final DeferredRegister<Schematic> SCHEMATICS = DeferredRegister.create(Schematic.class, Tardis.MODID);
	
	public static Supplier<IForgeRegistry<Schematic>> SCHEMATIC_REGISTRY = SCHEMATICS.makeRegistry("schematic", () -> new RegistryBuilder<Schematic>().setMaxID(Integer.MAX_VALUE - 1));
	
	public static final RegistryObject<Schematic> POLICE_BOX = SCHEMATICS.register("police_box", () -> new ExteriorUnlockSchematic(() -> ExteriorRegistry.POLICE_BOX.get()));
	public static final RegistryObject<Schematic> POLICE_BOX_MODERN = SCHEMATICS.register("police_box_modern", () -> new ExteriorUnlockSchematic(() -> ExteriorRegistry.MODERN_POLICE_BOX.get()));
	
	public static final RegistryObject<Schematic> TOYOTA_INTERIOR = SCHEMATICS.register("toyota_interior", () -> new InteriorUnlockSchematic(() -> ConsoleRoom.TOYOTA));
	public static final RegistryObject<Schematic> CORAL_INTERIOR = SCHEMATICS.register("coral_interior", () -> new InteriorUnlockSchematic(() -> ConsoleRoom.CORAL));

}
