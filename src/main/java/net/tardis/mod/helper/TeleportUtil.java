package net.tardis.mod.helper;

import java.util.LinkedList;

import net.minecraft.entity.Entity;
import net.minecraft.entity.item.minecart.MinecartEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.play.server.SPlayEntityEffectPacket;
import net.minecraft.network.play.server.SPlayerAbilitiesPacket;
import net.minecraft.network.play.server.SRespawnPacket;
import net.minecraft.network.play.server.SServerDifficultyPacket;
import net.minecraft.network.play.server.SSetPassengersPacket;
import net.minecraft.potion.EffectInstance;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.management.PlayerList;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeManager;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.IWorldInfo;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.fml.hooks.BasicEventHooks;

public class TeleportUtil {
	
	
    public static Entity teleportEntity(Entity entity, ServerWorld dimension, double xCoord, double yCoord, double zCoord, float yaw, float pitch){
        if (entity == null || entity.world.isRemote)
            return entity;

        MinecraftServer server = entity.getServer();
        ServerWorld sourceDim = entity.getServer().getWorld(dimension.getDimensionKey());

        if (!entity.isBeingRidden() || !entity.isPassenger()){
            return handleEntityTeleport(entity, server, sourceDim, dimension, xCoord, yCoord, zCoord, yaw, pitch);
        }

        final Entity rootEntity = entity.getLowestRidingEntity();
        final PassengerHelper passengerHelper = new PassengerHelper(rootEntity);
        final PassengerHelper rider = passengerHelper.getPassenger(entity);
        if (rider == null) {
            return entity;
        }
        passengerHelper.teleport(server, sourceDim, dimension, xCoord, yCoord, zCoord, yaw, pitch);
        passengerHelper.remountRiders();
        passengerHelper.updateClients();
        return rider.entity;
    }

    public static Entity teleportEntity(Entity entity, ServerWorld dimension, double xCoord, double yCoord, double zCoord) {
        return teleportEntity(entity, dimension, xCoord, yCoord, zCoord, entity.rotationYaw, entity.rotationPitch);
    }

    private static Entity handleEntityTeleport(Entity entity, MinecraftServer server, ServerWorld sourceDim, ServerWorld targetDim, double xCoord, double yCoord, double zCoord, float yaw, float pitch) {
        if (entity == null || entity.world.isRemote()) {
            return entity;
        }
        boolean interDimensional = sourceDim != targetDim;
        if (interDimensional && !ForgeHooks.onTravelToDimension(entity, targetDim.getDimensionKey())) {
            return entity;
        }
        if (!interDimensional) {
            if (entity instanceof ServerPlayerEntity) {
                ServerPlayerEntity player = (ServerPlayerEntity)entity;
                player.connection.setPlayerLocation(xCoord, yCoord, zCoord, yaw, pitch);
                player.setRotationYawHead(yaw);
            }
            else {
                entity.setLocationAndAngles(xCoord, yCoord, zCoord, yaw, pitch);
                entity.setRotationYawHead(yaw);
            }
            return entity;
        }
        if (entity instanceof ServerPlayerEntity) {
            return teleportPlayerInterdimentional((ServerPlayerEntity)entity, server, sourceDim, targetDim, xCoord, yCoord, zCoord, yaw, pitch);
        }
        return teleportEntityInterdimentional(entity, server, sourceDim, targetDim, xCoord, yCoord, zCoord, yaw, pitch);
    }

    private static Entity teleportEntityInterdimentional(Entity entity, MinecraftServer server, ServerWorld sourceDim, ServerWorld targetDim, double xCoord, double yCoord, double zCoord, float yaw, float pitch) {
        if (!entity.isAlive()) {
            return null;
        }
        final ServerWorld sourceWorld = sourceDim;
        final ServerWorld targetWorld = targetDim;
        if (entity.isAlive() && entity instanceof MinecartEntity) {
            entity.remove(true); //equivalent of old.removed = true;
            entity.changeDimension(targetDim);
            entity.revive(); //equivalent of old.removed = false;
        }
        entity.world = targetDim;
        sourceWorld.removeEntity(entity);
        entity.revive();
        entity.setLocationAndAngles(xCoord, yCoord, zCoord, yaw, pitch);
        sourceWorld.updateEntity(entity);
        final Entity newEntity = entity.getType().create((World)targetWorld);
        if (newEntity != null) {
            newEntity.copyDataFromOld(entity);
            newEntity.setLocationAndAngles(xCoord, yCoord, zCoord, yaw, pitch);
            final boolean flag = newEntity.forceSpawn;
            newEntity.forceSpawn = true;
            targetWorld.addEntity(newEntity);
            newEntity.forceSpawn = flag;
            targetWorld.updateEntity(newEntity);
        }
        entity.remove(true);
        sourceWorld.resetUpdateEntityTick();
        targetWorld.resetUpdateEntityTick();
        return newEntity;
    }

    private static PlayerEntity teleportPlayerInterdimentional(ServerPlayerEntity player, MinecraftServer server, ServerWorld sourceDim, ServerWorld destination, double xCoord, double yCoord, double zCoord, float yaw, float pitch) {
    	ServerWorld currentWorld = player.getServerWorld();
        if (currentWorld.getDimensionKey() == World.OVERWORLD && destination.getDimensionKey() == World.OVERWORLD) {
            player.detach();
            player.getServerWorld().removePlayer(player);
            if (!player.queuedEndExit) {
                player.queuedEndExit = true;
            }
            return player;
        }
        ServerWorld serverworld = currentWorld;
        player.world = destination;
        ServerWorld serverworld2 = destination;
        IWorldInfo worldinfo = player.world.getWorldInfo();
        player.connection.sendPacket(new SRespawnPacket(destination.getDimensionType(), destination.getDimensionKey(), BiomeManager.getHashedSeed(destination.getSeed()), player.interactionManager.getGameType(), player.interactionManager.func_241815_c_(), destination.isDebug(), destination.isFlatWorld(), true));
        player.connection.sendPacket(new SServerDifficultyPacket(worldinfo.getDifficulty(), worldinfo.isDifficultyLocked()));
        PlayerList playerlist = player.server.getPlayerList();
        playerlist.updatePermissionLevel(player);
        serverworld.removeEntity(player, true);
        player.revive();
//        double d0 = player.getPosX();
//        double d2 = player.getPosY();
//        double d3 = player.getPosZ();
        float f = player.rotationPitch;
        float f2 = player.rotationYaw;
//        double d4 = 8.0;
//        float f3 = f2;
        serverworld.getProfiler().startSection("moving");
        
        serverworld.getProfiler().endSection();
        player.setLocationAndAngles(xCoord, yCoord, zCoord, yaw, pitch);
        player.setMotion(Vector3d.ZERO);
        player.setWorld(serverworld2);
        serverworld2.addDuringPortalTeleport(player);
        player.func_213846_b(serverworld);
        player.connection.setPlayerLocation(player.getPosX(), player.getPosY(), player.getPosZ(), f2, f);
        player.interactionManager.setWorld(serverworld2);
        player.connection.sendPacket(new SPlayerAbilitiesPacket(player.abilities));
        playerlist.sendWorldInfo(player, serverworld2);
        playerlist.sendInventory(player);
        for (final EffectInstance effectinstance : player.getActivePotionEffects()) {
            player.connection.sendPacket(new SPlayEntityEffectPacket(player.getEntityId(), effectinstance));
        }
        //player.connection.sendPacket(new SPlaySoundEventPacket(1032, BlockPos.ZERO, 0, false));
        BasicEventHooks.firePlayerChangedDimensionEvent(player, currentWorld.getDimensionKey(), destination.getDimensionKey());
        return player;
    }

    public static Entity getHighestRidingEntity(final Entity mount) {
        Entity entity;
        for (entity = mount; entity.getPassengers().size() > 0; entity = entity.getPassengers().get(0)) {}
        return entity;
    }


    private static class PassengerHelper {
        public Entity entity;
        public LinkedList<PassengerHelper> passengers;
        public double offsetX;
        public double offsetY;
        public double offsetZ;

        public PassengerHelper(final Entity entity) {
            this.passengers = new LinkedList<PassengerHelper>();
            this.entity = entity;
            if (entity.isPassenger()) {
                this.offsetX = entity.getPosX() - entity.getRidingEntity().getPosX();
                this.offsetY = entity.getPosY() - entity.getRidingEntity().getPosY();
                this.offsetZ = entity.getPosZ() - entity.getRidingEntity().getPosZ();
            }
            for (final Entity passenger : entity.getPassengers()) {
                this.passengers.add(new PassengerHelper(passenger));
            }
        }

        public void teleport(final MinecraftServer server, final ServerWorld sourceDim, final ServerWorld targetDim, final double xCoord, final double yCoord, final double zCoord, final float yaw, final float pitch) {
            this.entity.removePassengers();
            this.entity = handleEntityTeleport(this.entity, server, sourceDim, targetDim, xCoord, yCoord, zCoord, yaw, pitch);
            for (final PassengerHelper passenger : this.passengers) {
                passenger.teleport(server, sourceDim, targetDim, xCoord, yCoord, zCoord, yaw, pitch);
            }
        }

        public void remountRiders() {
            if (this.entity.isPassenger()) {
                this.entity.setLocationAndAngles(this.entity.getPosX() + this.offsetX, this.entity.getPosY() + this.offsetY, this.entity.getPosZ() + this.offsetZ, this.entity.rotationYaw, this.entity.rotationPitch);
            }
            for (final PassengerHelper passenger : this.passengers) {
                passenger.entity.startRiding(this.entity, true);
                passenger.remountRiders();
            }
        }

        public void updateClients() {
            if (this.entity instanceof ServerPlayerEntity) {
                this.updateClient((ServerPlayerEntity)this.entity);
            }
            for (final PassengerHelper passenger : this.passengers) {
                passenger.updateClients();
            }
        }

        private void updateClient(final ServerPlayerEntity playerMP) {
            if (this.entity.isBeingRidden()) {
                playerMP.connection.sendPacket(new SSetPassengersPacket(this.entity));
            }
            for (final PassengerHelper passenger : this.passengers) {
                passenger.updateClients();
            }
        }

        public PassengerHelper getPassenger(final Entity passenger) {
            if (this.entity == passenger) {
                return this;
            }
            for (final PassengerHelper rider : this.passengers) {
                final PassengerHelper re = rider.getPassenger(passenger);
                if (re != null) {
                    return re;
                }
            }
            return null;
        }
    }
}
