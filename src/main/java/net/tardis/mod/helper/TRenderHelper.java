package net.tardis.mod.helper;

import static net.minecraft.client.renderer.WorldRenderer.drawVoxelShapeParts;

import java.nio.FloatBuffer;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nonnull;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.client.event.DrawHighlightEvent;
import net.minecraftforge.energy.CapabilityEnergy;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.items.DebugItem;
import net.tardis.mod.items.PlasmicShellItem;
import net.tardis.mod.items.SquarenessGunItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.items.TardisDiagnosticItem;
import net.tardis.mod.items.misc.IAttunable;
import net.tardis.mod.subsystem.SubsystemInfo;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.tileentities.machines.TransductionBarrierTile;

/**
 * Created by Swirtzly
 * on 25/03/2020 @ 13:03
 */
public class TRenderHelper {

    private static float lastBrightnessX = GlStateManager.lastBrightnessX;
    private static float lastBrightnessY = GlStateManager.lastBrightnessY;

    private static final FloatBuffer FLOAT_4_BUFFER = GLAllocation.createDirectFloatBuffer(4);


    public static void setLightmapTextureCoords(float x, float y) {
        lastBrightnessX = GlStateManager.lastBrightnessX;
        lastBrightnessY = GlStateManager.lastBrightnessY;
        GL13.glMultiTexCoord2f(GL13.GL_TEXTURE1, x, y);
    }

    public static Minecraft mc() {
	    return Minecraft.getInstance();
	}

    /**
     * Gets partialTicks for client side rendering
     * @return
     */
    public static float getRenderPartialTicks() {
	    return mc().getRenderPartialTicks();
	}

    public static void restoreLightMap() {
    	GL13.glMultiTexCoord2f(GL13.GL_TEXTURE1, lastBrightnessX, lastBrightnessY);
    }


    public static void renderFilledBox(Matrix4f matrix, IVertexBuilder builder, AxisAlignedBB boundingBox, float red, float green, float blue, float alpha, int combinedLightIn) {
        renderFilledBox(matrix, builder, (float) boundingBox.minX, (float) boundingBox.minY, (float) boundingBox.minZ, (float) boundingBox.maxX, (float) boundingBox.maxY, (float) boundingBox.maxZ, red, green, blue, alpha, combinedLightIn);
    }

    public static void renderFilledBox(Matrix4f matrix, IVertexBuilder builder, float startX, float startY, float startZ, float endX, float endY, float endZ, float red, float green, float blue, float alpha, int combinedLightIn) {
        //down
        builder.pos(matrix, startX, startY, startZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, startY, startZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, startY, endZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, startX, startY, endZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();

        //up
        builder.pos(matrix, startX, endY, startZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, startX, endY, endZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, endY, endZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, endY, startZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();

        //east
        builder.pos(matrix, startX, startY, startZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, startX, endY, startZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, endY, startZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, startY, startZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();

        //west
        builder.pos(matrix, startX, startY, endZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, startY, endZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, endY, endZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, startX, endY, endZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();

        //south
        builder.pos(matrix, endX, startY, startZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, endY, startZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, endY, endZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, startY, endZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();

        //north
        builder.pos(matrix, startX, startY, startZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, startX, startY, endZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, startX, endY, endZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, startX, endY, startZ).color(red, green, blue, alpha).lightmap(combinedLightIn).endVertex();
    }

	public static void renderInsideBox(int minX, int minY, int minZ, int maxX, int maxY, int maxZ) {
		BufferBuilder builder = Tessellator.getInstance().getBuffer();
		builder.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		
		float per = Minecraft.getInstance().world.getGameTime() % 200 / 200.0F;
		float maxU = 1, maxV = per + 0.25F;
		float minV = per;
		
		builder.pos(minX, minY, minZ).tex(0, minV).endVertex();
		builder.pos(maxX, minY, minZ).tex(maxU, minV).endVertex();
		builder.pos(maxX, maxY, minZ).tex(maxU, maxV).endVertex();
		builder.pos(minX, maxY, minZ).tex(0, maxV).endVertex();
		
		builder.pos(minX, minY, maxZ).tex(0, minV).endVertex();
		builder.pos(minX, maxY, maxZ).tex(0, maxV).endVertex();
		builder.pos(maxX, maxY, maxZ).tex(maxU, maxV).endVertex();
		builder.pos(maxX, minY, maxZ).tex(maxU, minV).endVertex();
		
		builder.pos(minX, minY, minZ).tex(0, minV).endVertex();
		builder.pos(minX, maxY, minZ).tex(0, maxV).endVertex();
		builder.pos(minX, maxY, maxZ).tex(maxU, maxV).endVertex();
		builder.pos(minX, minY, maxZ).tex(maxU, minV).endVertex();
		
		builder.pos(maxX, minY, minZ).tex(0, minV).endVertex();
		builder.pos(maxX, minY, maxZ).tex(maxU, minV).endVertex();
		builder.pos(maxX, maxY, maxZ).tex(maxU, maxV).endVertex();
		builder.pos(maxX, maxY, minZ).tex(0, maxV).endVertex();
		
		Tessellator.getInstance().draw();
	}

    public static void drawSelectionBoxMask(AxisAlignedBB box, float red, float green, float blue, float alpha) {
        drawMask(box.minX, box.minY, box.minZ, box.maxX, box.maxY, box.maxZ, red, green, blue, alpha);
    }

    public static void drawMask(double minX, double minY, double minZ, double maxX, double maxY, double maxZ, float red, float green, float blue, float alpha) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_COLOR);
        drawMask(bufferbuilder, minX, minY, minZ, maxX, maxY, maxZ, red, green, blue, alpha);
        tessellator.draw();
    }

    public static void drawMask(BufferBuilder b, double minX, double minY, double minZ, double maxX, double maxY, double maxZ, float red, float green, float blue, float alpha) {
        // up
        b.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, minY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();

        // down
        b.pos(minX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, maxY, minZ).color(red, green, blue, alpha).endVertex();

        // north
        b.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, minY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();

        // south
        b.pos(minX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, minY, maxZ).color(red, green, blue, alpha).endVertex();

        // east
        b.pos(maxX, minY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, minY, minZ).color(red, green, blue, alpha).endVertex();

        // west
        b.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();
    }
    
    /**
     * Draws a line around the specified AABB box
     * <br> Use {@link RenderType#getLines()} for the IVertexBuilder
     * @param matrixStack
     * @param builder
     * @param box
     * @param red
     * @param green
     * @param blue
     * @param alpha
     */
	public static void renderAABB(MatrixStack matrixStack, IVertexBuilder builder, AxisAlignedBB box, float red, float green, float blue, float alpha) {
		
		Matrix4f matrix4f = matrixStack.getLast().getMatrix();
		float boxMinX = (float)box.minX;
	    float boxMinY = (float)box.minY;
	    float boxMinZ = (float)box.minZ;
	    float boxMaxX = (float)box.maxX;
	    float boxMaxY = (float)box.maxY;
	    float boxMaxZ = (float)box.maxZ;
		builder.pos(matrix4f, boxMinX, boxMinY, boxMaxZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMaxX, boxMinY, boxMaxZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMaxX, boxMaxY, boxMaxZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMinX, boxMaxY, boxMaxZ).color(red, green, blue, alpha).endVertex();
		
		builder.pos(matrix4f, boxMinX, boxMinY, boxMinZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMinX, boxMaxY, boxMinZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMaxX, boxMaxY, boxMinZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMaxX, boxMinY, boxMinZ).color(red, green, blue, alpha).endVertex();
		
		builder.pos(matrix4f, boxMinX, boxMinY, boxMinZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMinX, boxMinY, boxMaxZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMinX, boxMaxY, boxMaxZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMinX, boxMaxY, boxMinZ).color(red, green, blue, alpha).endVertex();
		
		builder.pos(matrix4f, boxMaxX, boxMinY, boxMinZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMaxX, boxMaxY, boxMinZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMaxX, boxMaxY, boxMaxZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMaxX, boxMinY, boxMaxZ).color(red, green, blue, alpha).endVertex();
		
		builder.pos(matrix4f, boxMinX, boxMinY, boxMinZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMaxX, boxMinY, boxMinZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMaxX, boxMinY, boxMaxZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMinX, boxMinY, boxMaxZ).color(red, green, blue, alpha).endVertex();
		
		builder.pos(matrix4f, boxMinX, boxMaxY, boxMinZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMinX, boxMaxY, boxMaxZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMaxX, boxMaxY, boxMaxZ).color(red, green, blue, alpha).endVertex();
		builder.pos(matrix4f, boxMaxX, boxMaxY, boxMinZ).color(red, green, blue, alpha).endVertex();
		
	}

	public static void renderSineWave(int x, int y, int width, int height, double amp) {
		BufferBuilder builder = Tessellator.getInstance().getBuffer();
		builder.begin(GL11.GL_LINES, DefaultVertexFormats.POSITION_COLOR);
		
		double t = System.currentTimeMillis() * 0.02;
		
		for(int point = 0; point < width; point++) {
			
			builder.pos(x + (point * 0.75), y + Math.sin((point + t) * amp) * height, 0).color(1F, 1, 1, 1).endVertex();
			builder.pos(x + (point + 1) * 0.75, y + Math.sin((point + t + 1) * amp) * height, 0).color(1F, 1, 1, 1).endVertex();
		}
		
		Tessellator.getInstance().draw();
	}


    /**
     * <a href="https://stackoverflow.com/a/41491220/10434371">Source</a>
     */
    public static double calculateColorBrightness(Vector3d c) {
        float r = (float) c.x, g = (float) c.y, b = (float) c.z;
        r = r <= 0.03928 ? r / 12.92F : (float) Math.pow((r + 0.055) / 1.055, 2.4);
        g = g <= 0.03928 ? g / 12.92F : (float) Math.pow((g + 0.055) / 1.055, 2.4);
        b = b <= 0.03928 ? b / 12.92F : (float) Math.pow((b + 0.055) / 1.055, 2.4);

        return (0.2126 * r) + (0.7152 * g) + (0.0722 * b);
    }

    public static void drawModelToGui(MatrixStack stack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha, EntityModel<ClientPlayerEntity> model, int xPos, int yPos, float scale, float rotation) {
        RenderSystem.pushMatrix();
        RenderSystem.enableDepthTest();
        RenderSystem.enableBlend();
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        RenderSystem.translatef(xPos, yPos, 100);
        RenderSystem.rotatef(-25, 1, 0, 0);
        RenderSystem.rotatef(rotation, 0, 1, 0);
        RenderHelper.setupGui3DDiffuseLighting();

        GlStateManager.lightModel(2899, getBuffer(0.75F, 0.75F, 0.75F, 1F));
        RenderSystem.scalef(38 * scale, 34 * scale, 38 * scale);
        RenderSystem.scalef(-1, 1, 1);
        model.render(stack, buffer, packedLight, packedOverlay, (float) red, (float) green,(float) blue, alpha);
        //model.render(Minecraft.getInstance().player, 0, 0, Minecraft.getInstance().player.ticksExisted, 0, 0, 0.0625f);
        RenderHelper.disableStandardItemLighting();
        RenderSystem.disableBlend();
        RenderSystem.disableDepthTest();
        RenderSystem.popMatrix();
    }

    public static void copyModelAngles(ModelRenderer src, ModelRenderer dest) {
        dest.rotateAngleX = src.rotateAngleX;
        dest.rotateAngleY = src.rotateAngleY;
        dest.rotateAngleZ = src.rotateAngleZ;
        dest.rotationPointX = src.rotationPointX;
        dest.rotationPointY = src.rotationPointY;
        dest.rotationPointZ = src.rotationPointZ;
    }


    public static void blit(int left, int top, int right, int bottom, float red, float green, float blue, float alpha) {
        if (left < right) {
            int i = left;
            left = right;
            right = i;
        }

        if (top < bottom) {
            int j = top;
            top = bottom;
            bottom = j;
        }

        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferBuilder = tessellator.getBuffer();
        RenderSystem.enableBlend();
        RenderSystem.disableTexture();
        RenderSystem.blendFuncSeparate(770, 771, 1, 0);
        RenderSystem.color4f((float) red, (float) green,(float) blue, alpha);
        bufferBuilder.begin(7, DefaultVertexFormats.POSITION);
        bufferBuilder.pos(left, bottom, 0.0D).endVertex();
        bufferBuilder.pos(right, bottom, 0.0D).endVertex();
        bufferBuilder.pos(right, top, 0.0D).endVertex();
        bufferBuilder.pos(left, top, 0.0D).endVertex();
        tessellator.draw();
        RenderSystem.enableTexture();
        RenderSystem.disableBlend();
    }

    private static FloatBuffer getBuffer(float float1, float float2, float float3, float float4) {
        FLOAT_4_BUFFER.clear();
        FLOAT_4_BUFFER.put(float1).put(float2).put(float3).put(float4);
        FLOAT_4_BUFFER.flip();
        return FLOAT_4_BUFFER;
    }

    public static void renderFilledBox(Matrix4f matrix, IVertexBuilder builder, AxisAlignedBB boundingBox, double red, double green, double blue, float alpha, int combinedLightIn) {
        renderFilledBox(matrix, builder, (float) boundingBox.minX, (float) boundingBox.minY, (float) boundingBox.minZ, (float) boundingBox.maxX, (float) boundingBox.maxY, (float) boundingBox.maxZ, (float) red, (float) green,(float) blue, alpha, combinedLightIn);
    }

    public static void renderFilledBox(Matrix4f matrix, IVertexBuilder builder, float startX, float startY, float startZ, float endX, float endY, float endZ, double red, double green, double blue, float alpha, int combinedLightIn) {
        //down
        builder.pos(matrix, startX, startY, startZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, startY, startZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, startY, endZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, startX, startY, endZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();

        //up
        builder.pos(matrix, startX, endY, startZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, startX, endY, endZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, endY, endZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, endY, startZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();

        //east
        builder.pos(matrix, startX, startY, startZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, startX, endY, startZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, endY, startZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, startY, startZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();

        //west
        builder.pos(matrix, startX, startY, endZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, startY, endZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, endY, endZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, startX, endY, endZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();

        //south
        builder.pos(matrix, endX, startY, startZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, endY, startZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, endY, endZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, endX, startY, endZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();

        //north
        builder.pos(matrix, startX, startY, startZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, startX, startY, endZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, startX, endY, endZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
        builder.pos(matrix, startX, endY, startZ).color((float) red, (float) green,(float) blue, alpha).lightmap(combinedLightIn).endVertex();
    }

    public static void drawGlowingLine(Matrix4f matrix, IVertexBuilder builder, float length, float width, double red, double green, double blue, float alpha, int combinedLightIn) {
        AxisAlignedBB box = new AxisAlignedBB(-width / 2F, 0, -width / 2F, width / 2F, length, width / 2F);
        renderFilledBox(matrix, builder, box, 1F, 1F, 1F, alpha, combinedLightIn);

        for(int i = 0; i < 3; i++) {
            renderFilledBox(matrix, builder, box.grow(i * 0.5F * 0.0625F), (float) red, (float) green,(float) blue, (1F / i / 2) * alpha, combinedLightIn);
        }
    }

    /**
     * Renders an itemstack using matrix stack
     * <br> Based off https://github.com/mekanism/Mekanism/blob/1.16.x/src/main/java/mekanism/client/gui/GuiUtils.java
     * @param matrix
     * @param renderer
     * @param stack
     * @param xAxis
     * @param yAxis
     * @param scale
     * @param font
     * @param text
     * @param overlay
     */
    public static void renderItem(MatrixStack matrix, net.minecraft.client.renderer.ItemRenderer renderer, @Nonnull ItemStack stack, int xAxis, int yAxis, float scale, FontRenderer font,
                                  String text, boolean overlay) {
        if (!stack.isEmpty()) {
            try {
                matrix.push();
                RenderSystem.enableDepthTest();
                RenderHelper.enableStandardItemLighting();
                if (scale != 1) {
                    RenderSystem.enableRescaleNormal();
                    matrix.scale(scale, scale, scale);
                    RenderSystem.disableRescaleNormal();
                }
                //Apply our matrix stack to the render system and pass an unmodified one to the render methods
                // Vanilla still renders the items using render system transformations so this is required to
                // have things render in the correct order
                RenderSystem.pushMatrix();
                RenderSystem.multMatrix(matrix.getLast().getMatrix());
                renderer.renderItemAndEffectIntoGUI(stack, xAxis, yAxis);
                if (overlay) {
                    renderer.renderItemOverlayIntoGUI(font, stack, xAxis, yAxis, text);
                }
                RenderSystem.popMatrix();
                RenderHelper.disableStandardItemLighting();
                RenderSystem.disableDepthTest();
                matrix.pop();
            } catch (Exception e) {
                Tardis.LOGGER.error("Failed to render stack into gui: {}", stack, e);
            }
        }
    }

    /**
     * Renders an ItemStack on top of a GUI Background
     * <br> Conduct Scaling and Rotation before calling this
     * @param matrixStack
     * @param itemRenderer
     * @param stack
     * @param translateX
     * @param translateY
     * @param translateZ
     */
    public static void renderItemOnScreenBackground(MatrixStack matrixStack, ItemRenderer itemRenderer, @Nonnull ItemStack stack, float translateX, float translateY, float translateZ) {

        IRenderTypeBuffer.Impl iRenderBufferImpl = Minecraft.getInstance().getRenderTypeBuffers().getBufferSource();
        int combinedLight = 15728880;
        int combinedOverlay = OverlayTexture.NO_OVERLAY;

        matrixStack.push();
        matrixStack.translate(translateX, translateY, translateZ);
        RenderSystem.pushMatrix();
        RenderSystem.multMatrix(matrixStack.getLast().getMatrix());

        itemRenderer.renderItem(stack, ItemCameraTransforms.TransformType.GUI, combinedLight, combinedOverlay, matrixStack, iRenderBufferImpl);

        RenderSystem.popMatrix();
        matrixStack.pop();
    }
    
    public static void drawDiagnosticText(PlayerEntity player, MatrixStack matrixStack, FontRenderer fr, int scaledWidth, int scaledHeight, DecimalFormat format) {
    	if (player != null) {
    		if (PlayerHelper.InEitherHand(player, stack -> stack.getItem() instanceof TardisDiagnosticItem)) {
                if (Minecraft.getInstance().objectMouseOver instanceof BlockRayTraceResult) {
                    TileEntity te = player.world.getTileEntity(((BlockRayTraceResult) Minecraft.getInstance().objectMouseOver).getPos());
                    if (te instanceof ExteriorTile) {
                        ExteriorTile ext = (ExteriorTile) te;
                        try {
                        	World interiorWorld = Minecraft.getInstance().world.func_241828_r().getRegistry(Registry.WORLD_KEY).getOrThrow(((ExteriorTile) te).getInterior());
                        	interiorWorld.getCapability(Capabilities.TARDIS_DATA).ifPresent(tardis -> {
                        		 String name = "Timeship Name: " + tardis.getTARDISName();
                        		 fr.drawStringWithShadow(matrixStack, name, scaledWidth / 2 - fr.getStringWidth(name) / 2, 70, 0xFFFFFF);
                        	});
                            
                        } catch (Exception e) {}
                    } else if (te instanceof ConsoleTile) {
                        player.getHeldItemMainhand().getCapability(Capabilities.DIAGNOSTIC).ifPresent(loc -> {
                            int i = 0;
                            int x = scaledWidth / 2 - 50;
                            int y = 0;
                            net.minecraft.client.renderer.RenderHelper.enableStandardItemLighting();
                            TranslationTextComponent subsystem_title = new TranslationTextComponent("overlay.tardis.system_title");
                            fr.drawStringWithShadow(matrixStack, subsystem_title.getString(), x - 20, y + 10, 0xFFFFFF);
                            for (SubsystemInfo info : loc.getSubsystems()) {
                                y = (int) (fr.FONT_HEIGHT * i * 1.5F) + 20;
                                float percent = info.health * 100.0F;

                                TextFormatting color = TextFormatting.GREEN;

                                if (percent <= 100) {
                                    color = TextFormatting.DARK_GREEN;

                                    if (percent <= 75) {
                                        color = TextFormatting.GREEN;

                                        if (percent <= 50) {
                                            color = TextFormatting.YELLOW;

                                            if (percent <= 25)
                                                color = TextFormatting.DARK_RED;
                                        }
                                    }
                                }
                                StringTextComponent percentToString = new StringTextComponent(format.format(percent) + "%");
                                fr.drawStringWithShadow(matrixStack, new TranslationTextComponent(info.translationKey).getString() + ": " +
                                        percentToString.mergeStyle(color).getString(), x, y, 0xFFFFFF);
                                Minecraft.getInstance().getItemRenderer()
                                        .renderItemAndEffectIntoGUI(new ItemStack(info.key), x - 16, y - 4);
                                ++i;
                            }
                            TranslationTextComponent tardisPower = new TranslationTextComponent("overlay.tardis.fe_power", loc.getPower());
                            fr.drawStringWithShadow(matrixStack, tardisPower.getString(), x, y + fr.FONT_HEIGHT + 10, 0xFFFFFF);
                            net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
                        });
                    }
                }
            }
    	} 
    }
    
    public static void drawPlayerCapabilityText(PlayerEntity player, MatrixStack matrixStack, FontRenderer fr, int scaledWidth, int scaledHeight) {
        if (player != null) {
        	if (PlayerHelper.isInEitherHand(player, TItems.SONIC.get())) {
                ItemStack stack = PlayerHelper.getHeldStack(player, Hand.MAIN_HAND);
                stack.getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(sonic -> {
                    if (sonic.getMode() == 0) { //If in Block Interaction Mode
                        RayTraceResult result = PlayerHelper.getPosLookingAt(player, 4);
                        if (result instanceof BlockRayTraceResult) {
                            BlockRayTraceResult btr = (BlockRayTraceResult) result;
                            TileEntity te = player.world.getTileEntity(btr.getPos());
                            if (te != null) {
                                te.getCapability(CapabilityEnergy.ENERGY).ifPresent(cap -> {
                                    int oldFE = sonic.getForgeEnergy();
                                    int maxStorage = cap.getMaxEnergyStored();
                                    int storage = cap.getEnergyStored();
                                    TranslationTextComponent energy = new TranslationTextComponent("message.tardis.energy_buffer", oldFE, maxStorage);
                                    if (oldFE != storage) {
                                        sonic.setForgeEnergy(storage); //Sets sonic's forge energy to new value. Sonic item will sync to client
                                    } else {
                                    	matrixStack.push();
                                        fr.drawStringWithShadow(matrixStack, energy.getString(), scaledWidth / 2 - 60, scaledHeight / 2 - 45, 0xFFFFFF);
                                        matrixStack.pop();
                                    }
                                });
                                if (te instanceof TransductionBarrierTile) {
                                    TransductionBarrierTile barrier = (TransductionBarrierTile) te;
                                    String code = barrier.getCode();
                                    TranslationTextComponent codeText = new TranslationTextComponent("message.tardis.transduction_barrier.code", code.isEmpty() ? "null" : code);
                                    matrixStack.push();
                                    fr.drawStringWithShadow(matrixStack,codeText.getString(), scaledWidth / 2 - 60, scaledHeight / 2 - 55, 0xFFFFFF);
                                    matrixStack.pop();
                                }
                            }
                        }
                    }
                });
            }
            player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
                if (cap.getCountdown() > 0) {
                    String countDown = "" + (cap.getCountdown() / 20);
                    int countWidth = fr.getStringWidth(countDown);
                    TranslationTextComponent roomDeletionMessage = new TranslationTextComponent("message.tardis.ars.room.delete_countdown", countDown);
                    fr.drawStringWithShadow(matrixStack,roomDeletionMessage.getString(), scaledWidth / 2 - countWidth / 2 - 100, scaledHeight / 2 - 30, 0xFFFFFF);
                }
            });
        }	
    }
    
    public static void drawTextAtLookVec(PlayerEntity player, MatrixStack matrixStack, FontRenderer fr, int scaledWidth, int scaledHeight) {
    	if (player != null) {
    		RayTraceResult result = PlayerHelper.getPosLookingAt(player, 4);
            if (result instanceof BlockRayTraceResult) {
                BlockRayTraceResult btr = (BlockRayTraceResult) result;
                BlockState state = player.world.getBlockState(btr.getPos());
                TileEntity te = player.world.getTileEntity(btr.getPos());
                if (te != null) {
//                	if (te instanceof AntiGravityTile) {
//                		AntiGravityTile grav = (AntiGravityTile)te;
//                		ITextComponent text = grav.getDisplayName();
//                		if (state.hasProperty(AntiGravBlock.ACTIVATED) && state.get(AntiGravBlock.ACTIVATED)) {
//                			fr.drawStringWithShadow(matrixStack, text.getString(), scaledWidth / 2 - 20, scaledHeight / 2 - 55, 0xFFFFFF);
//                		}
//                	}
                }
            }
    	}
    }
    
    public static void drawDebugBoundingBoxes(PlayerEntity player, MatrixStack matrixStack, IVertexBuilder builder, double correctedRenderPosX, double correctedRenderPosY, double correctedRenderPosZ) {
    	if (player != null) {
    		ItemStack main = player.getHeldItemMainhand();
    		if (main.getItem() == TItems.PLASMIC_SHELL_GENERATOR.get()) {
            	matrixStack.push();
                matrixStack.translate(-correctedRenderPosX, -correctedRenderPosY, -correctedRenderPosZ);
                
                if (player.getHeldItemMainhand().getOrCreateTag().contains("pos1")) {

                    BlockPos pos = BlockPos.fromLong(main.getOrCreateTag().getLong("pos1"));
                    BlockPos pos1 = main.getOrCreateTag().contains("pos2") ? BlockPos.fromLong(main.getOrCreateTag().getLong("pos2")) : player.getPosition();

                    boolean green = main.getOrCreateTag().contains("complete") && main.getOrCreateTag().getBoolean("complete");
                    WorldRenderer.drawBoundingBox(matrixStack, builder, PlasmicShellItem.getBox(pos, pos1).grow(0.1), !green ? 1 : 0, green ? 1 : 0, 0, 0.5F);
                }
                matrixStack.pop();

            }
    		if (main.getItem() == TItems.DEBUG.get()) {
    			matrixStack.push();
                matrixStack.translate(-correctedRenderPosX, -correctedRenderPosY, -correctedRenderPosZ);
            	WorldRenderer.drawBoundingBox(matrixStack, builder, ((DebugItem) TItems.DEBUG.get()).box, 1, 0, 0, 0.5F);
            	matrixStack.pop();
    		}
    	}
    }
    
    public static void drawSquarenessGunOutline(PlayerEntity player, MatrixStack matrixStack, ActiveRenderInfo information, DrawHighlightEvent event) {
    	if (player != null) {
    		if (PlayerHelper.isInEitherHand(player, TItems.SQUARENESS_GUN.get())) {
                event.setCanceled(true);
                RayTraceResult result = PlayerHelper.getPosLookingAt(player, 5);
                if (result != null && result.getType() == RayTraceResult.Type.BLOCK) {
                    BlockRayTraceResult blockRayTraceResult = (BlockRayTraceResult) result;
                    BlockPos pos = blockRayTraceResult.getPos();
                    ClientWorld world = Minecraft.getInstance().world;

                    final Vector3d projectedView = information.getProjectedView();
                    double correctedRenderPosX = projectedView.getX();
                    double correctedRenderPosY = projectedView.getY();
                    double correctedRenderPosZ = projectedView.getZ();

                    //Create a cube that we can just grow one in every direction
                    AxisAlignedBB box = SquarenessGunItem.getSelection(pos, player.getHorizontalFacing());

                    matrixStack.push();
                    matrixStack.translate(correctedRenderPosX, correctedRenderPosY, correctedRenderPosZ);
                    for (int iteratedX = (int) box.minX; iteratedX < (int) box.maxX; iteratedX++) {
                        for (int iteratedY = (int) box.minY; iteratedY < (int) box.maxY; iteratedY++) {
                            for (int iteratedZ = (int) box.minZ; iteratedZ < (int) box.maxZ; iteratedZ++) {
                                if (!world.isAirBlock(pos.add(iteratedX, iteratedY, iteratedZ))) {
                                	matrixStack.push();
                                	matrixStack.translate(iteratedX, iteratedY, iteratedZ);
                                    BlockState blockState = world.getBlockState(pos.add(iteratedX, iteratedY, iteratedZ));
                                    VoxelShape collisionShape = blockState.getCollisionShape(world, pos);
                                    if (!collisionShape.isEmpty()) {
                                        TRenderHelper.drawSelectionBoxMask(collisionShape.getBoundingBox(), 0.2F, 0.6F, 0.8F, 0.9F);
                                    }
                                    matrixStack.pop();
                                }
                            }
                        }
                    }
                    matrixStack.pop();

                    double projectedX = information.getProjectedView().x;
                    double projectedY = information.getProjectedView().y;
                    double projectedZ = information.getProjectedView().z;
                    for (Iterator<BlockPos> iterator = BlockPos.getAllInBox(new BlockPos(box.maxX, box.maxY, box.maxZ), new BlockPos(box.minX, box.minY, box.minZ)).iterator(); iterator.hasNext(); ) {
                        BlockPos blockPos = iterator.next();
                        BlockState shape = player.world.getBlockState(blockPos);
                        IVertexBuilder ivertexbuilder = event.getBuffers().getBuffer(RenderType.getLines());
                        drawVoxelShapeParts(matrixStack, ivertexbuilder,  shape.getShape((ClientWorld) player.world, blockPos, ISelectionContext.forEntity(information.getRenderViewEntity())), (double) blockPos.getX() - projectedX, (double) blockPos.getY() - projectedY, (double) blockPos.getZ() - projectedZ, 0.0F, 0.0F, 1, 1F);
                    }
                }

            }
    	}
    }
    
    public static void handleTooltipRendering(ItemStack stack, List<ITextComponent> tooltips, ITooltipFlag flag, PlayerEntity player) {
    	//Add attunment tooltips to an itemstack which was attuned via a recipe and is not handling its own tooltip logic
    	//E.g. In the future we make a new item that doesn't need to implement IAttunable, and we just make an attunable recipe for it, and tell the recipe to add tardis name and world key nbt tags to it
    	//This will automatically add the tooltip to the item without needing us to manually handle it
    	if (stack != null && player != null) {
			if (!(stack.getItem() instanceof IAttunable)) { //If the stack isn't implementing IAttunable and handling its own custom logic, which usually includes its own tooltip logic
				if(stack.getTag() != null) {//If the stack has the attunement tags added (i.e. it's been attuned)
    				if (stack.getTag().contains(Constants.CONSOLE_ATTUNMENT_NBT_KEY)) {
            			if (stack.getTag().contains(Constants.TARDIS_NAME_ATTUNMENT_NBT_KEY)) {
            				String tardisName = stack.getTag().getString(Constants.TARDIS_NAME_ATTUNMENT_NBT_KEY);
            				if (tardisName != null) {
            					tooltips.add(1, new TranslationTextComponent(Constants.Translations.TOOLTIP_ATTUNED_OWNER).appendSibling(new StringTextComponent(tardisName).mergeStyle(TextFormatting.LIGHT_PURPLE)));
            				}
            			}
            		}
				}
		    }
    	}
    }

}
