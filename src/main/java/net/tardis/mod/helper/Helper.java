package net.tardis.mod.helper;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.advancements.Advancement;
import net.minecraft.block.BlockState;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.minecart.MinecartEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.projectile.FishingBobberEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootContext;
import net.minecraft.loot.LootParameterSets;
import net.minecraft.loot.LootParameters;
import net.minecraft.loot.LootTable;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.play.server.SPlayEntityEffectPacket;
import net.minecraft.network.play.server.SPlayerAbilitiesPacket;
import net.minecraft.network.play.server.SServerDifficultyPacket;
import net.minecraft.network.play.server.SSetExperiencePacket;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.server.MinecraftServer;
import net.minecraft.state.Property;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.Util;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.DimensionType;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.server.TicketType;
import net.minecraft.world.storage.IWorldInfo;
import net.minecraftforge.common.world.ForgeChunkManager;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.constants.Constants.Translations;
import net.tardis.mod.containers.BaseContainer;
import net.tardis.mod.experimental.advancement.TTriggers;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.ShipComputerTile;
import net.tardis.mod.world.dimensions.TDimensions;
/** Generic Helper class*/
public class Helper {

    private static Random rand = new Random();

    /**
     * <p> DEPRECATED: Moved to WorldHelper
     * Compares the current world's dimension type against a registry key of a json based dimension type
     * @param world
     * @param dimTypeKey
     * @return True if the registry keys match, false if they do not
     */
    @Deprecated
    public static boolean areDimensionTypesSame(World world, RegistryKey<DimensionType> dimTypeKey) {
    	RegistryKey<DimensionType> worldDimKey = world.func_241828_r().func_230520_a_().getOptionalKey(world.getDimensionType()).orElse(DimensionType.OVERWORLD);
        return worldDimKey == dimTypeKey;
    }

	public static boolean isInBounds(int testX, int testY, int x, int y, int u, int v) {
		return (testX > x &&
				testX < u &&
				testY > y &&
				testY < v);
	}

    public static void doIfAdvancementPresent(String name, ServerPlayerEntity ent, Runnable run) {
    	Advancement adv = ent.getServer().getAdvancementManager().getAdvancement(new ResourceLocation(Tardis.MODID, name));
    	if(adv != null) {
    		if(ent.getAdvancements().getProgress(adv).isDone()) {
    			run.run();
    		}
    	}
    }
    /**
     * Run if a specified advancement has been obtained (Works for other mods or Vanilla MC)
     * @param name
     * @param ent
     * @param run
     */
    public static void doIfAdvancementPresentOther(ResourceLocation loc, ServerPlayerEntity ent, Runnable run) {
    	Advancement adv = ent.getServer().getAdvancementManager().getAdvancement(loc);
    	if(adv != null) {
    		if(ent.getAdvancements().getProgress(adv).isDone()) {
    			run.run();
    		}
    	}
    }

	public static boolean unlockInterior(ConsoleTile console, @Nullable ServerPlayerEntity player, ConsoleRoom room) {
		if(!console.getUnlockManager().getUnlockedConsoleRooms().contains(room)) {
			console.getUnlockManager().addConsoleRoom(room);
			if(player != null) {
				TTriggers.INTERIOR_UNLOCK.trigger(player);
				player.sendStatusMessage(new TranslationTextComponent(Translations.UNLOCKED_INTERIOR, room.getDisplayName()), true);
			}
			return true;
		}
		return false;
	}
	
	public static boolean unlockExterior(ConsoleTile console, @Nullable ServerPlayerEntity player, AbstractExterior ext) {
		if(!console.getUnlockManager().getUnlockedExteriors().contains(ext)) {
			console.getUnlockManager().addExterior(ext);
			if(player != null) {
				TTriggers.EXTERIOR_UNLOCK.trigger(player);
				player.sendStatusMessage(new TranslationTextComponent(Translations.UNLOCKED_EXTERIOR, ext.getDisplayName().getString()), true);
			}
			return true;
		}
		return false;
	}

	public static ResourceLocation createRL(String string) {
		return new ResourceLocation(Tardis.MODID, string);
	}
	
	public static String createRLString(String name) {
    	return Tardis.MODID + ":" + name;
    }
	
	/**
	 * Tests if a blockstate is blacklisted from rendering in BOTI applications
	 * @param state
	 * @return false if blacklisted, true if allowed
	 */
	public static boolean canRenderInBOTI(BlockState state) {
		for(String blocked : TConfig.CLIENT.botiBlacklistedBlocks.get()) {
			if(state.getBlock().getRegistryName().toString().equals(blocked))
				return false;
			if(blocked.endsWith("*")) {
				String modid = blocked.substring(0, blocked.indexOf(':'));
				if(state.getBlock().getRegistryName().getNamespace().equals(modid))
					return false;
			}
		}
		return true;
	}
	
	/**
     * Tests if an entity is blacklisted from rendering in BOTI applications
     * @param entity
     * @return false if blacklisted, true if allowed
     */
    public static boolean canRenderInBOTI(Entity entity) {
        for(String blocked : TConfig.CLIENT.botiBlacklistedEntities.get()) {
            if(entity.getType().getRegistryName().toString().equals(blocked))
                return false;
            if(blocked.endsWith("*")) {
                String modid = blocked.substring(0, blocked.indexOf(':'));
                if(entity.getType().getRegistryName().getNamespace().equals(modid))
                    return false;
            }
        }
        return true;
    }

	public static <T extends Object> void addAllToListNotDuplicate(ArrayList<T> list1, List<T> list2) {
		for(T obj : list2) {
			if(!list1.contains(obj))
				list1.add(obj);
		}
	}

	public static <T extends INBT> List<T> getListFromNBT(ListNBT nbt, Class<T> nbtClass) {
		List<T> list = Lists.newArrayList();
		for(INBT n : nbt) {
			list.add(nbtClass.cast(n));
		}
		return list;
	}

	public static String[] getConsoleText(ConsoleTile console) {
		
		if(console.getFlightEvent() != null)
			return new String[] {
					"",
					console.getFlightEvent().getTranslation().getString()
					};
		
		if(console.getInteriorManager().getMonitorOverrides() != null)
			return console.getInteriorManager().getMonitorOverrides().getText();
		
		boolean hasNav = console.hasNavCom();
		
		return new String[] {
		        Constants.Translations.LOCATION.getString() + (hasNav ? WorldHelper.formatBlockPos(console.getCurrentLocation()): "UNKNOWN"),
				Constants.Translations.DIMENSION.getString() + (hasNav ? WorldHelper.formatDimName(console.getCurrentDimension()) : "UNKNOWN"),
				Constants.Translations.FACING.getString() + console.getExteriorFacingDirection().getName2().toUpperCase(),
				Constants.Translations.TARGET.getString() + (hasNav ? WorldHelper.formatBlockPos(console.getDestinationPosition()) : "UNKNOWN"),
				Constants.Translations.TARGET_DIM.getString() + (hasNav ? WorldHelper.formatDimName(console.getDestinationDimension()) : "UNKNOWN"),
				Constants.Translations.ARTRON.getString() + Constants.TEXT_FORMAT_NO_DECIMALS.format(console.getArtron()) + "AU",
				Constants.Translations.JOURNEY.getString() + Constants.TEXT_FORMAT_NO_DECIMALS.format(console.getPercentageJourney() * 100.0F) + "%"
				};
	}

	public static AxisAlignedBB createBBWithRaduis(int i) {
		return new AxisAlignedBB(-i, -i, -i, i, i, i);
	}
	
	
	/**
	 * Cycles a Blockstate's Property. Derived from DebugStickItem#cycleProperty
	 * <br> Use this to replace Blockstate#cycle as this was removed in 1.16
	 * @param <T>
	 * @param state
	 * @param propertyIn
	 * @param backwards
	 * @return
	 */
	public static <T extends Comparable<T>> BlockState cycleBlockStateProperty(BlockState state, Property<T> propertyIn) {
	      return state.with(propertyIn, getAdjacentValue(propertyIn.getAllowedValues(), state.get(propertyIn), false));
	}
	
	/**
	 * Cycles a Blockstate's Property. Derived from DebugStickItem#cycleProperty
	 * <br> Use this to replace Blockstate#cycle as this was removed in 1.16
	 * @param <T>
	 * @param state
	 * @param propertyIn
	 * @param backwards
	 * @return
	 */
	public static <T extends Comparable<T>> BlockState cycleBlockStateProperty(BlockState state, Property<T> propertyIn, boolean backwards) {
	      return state.with(propertyIn, getAdjacentValue(propertyIn.getAllowedValues(), state.get(propertyIn), backwards));
	}
	
	private static <T> T getAdjacentValue(Iterable<T> allowedValues, @Nullable T currentValue, boolean backwards) {
	      return (T)(backwards ? Util.getElementBefore(allowedValues, currentValue) : Util.getElementAfter(allowedValues, currentValue));
	}
	
	// helper for making the above private field getters via reflection
	@SuppressWarnings("unchecked") // also throws ClassCastException if the types are wrong
	static <FIELDHOLDER,FIELDTYPE> Function<FIELDHOLDER,FIELDTYPE> getInstanceField(Class<FIELDHOLDER> fieldHolderClass, String fieldName)
	{
		// forge's ORH is needed to reflect into vanilla minecraft java
		Field field = ObfuscationReflectionHelper.findField(fieldHolderClass, fieldName);
		
		return instance -> {
			try
			{
				return (FIELDTYPE)(field.get(instance));
			}
			catch (IllegalArgumentException | IllegalAccessException e)
			{
				throw new RuntimeException(e);
			}
		};
	}
	
	public static void addLootToComputerBelow(IServerWorld worldIn, BlockPos pos, ResourceLocation lootRL) {
		TileEntity te = worldIn.getTileEntity(pos.down());
		if (te instanceof ShipComputerTile) {
			 ShipComputerTile computer = (ShipComputerTile) te;
			 computer.setLootTable(lootRL);
			 worldIn.removeBlock(pos, false);
		}
	}

}