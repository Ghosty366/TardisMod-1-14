package net.tardis.mod.compat.jei;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.registration.IGuiHandlerRegistration;
import mezz.jei.api.registration.IRecipeCategoryRegistration;
import mezz.jei.api.registration.IRecipeRegistration;
import net.minecraft.client.Minecraft;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.containers.AlembicScreen;
import net.tardis.mod.client.guis.containers.QuantiscopeWeldScreen;
import net.tardis.mod.compat.jei.ARSRecipeCategory.ARSRecipe;
import net.tardis.mod.items.misc.IAttunable;
import net.tardis.mod.recipe.AlembicRecipe;
import net.tardis.mod.recipe.AttunableRecipe;
import net.tardis.mod.recipe.AttunableRecipe.RecipeResult;
import net.tardis.mod.recipe.WeldRecipe;
import net.tardis.mod.tags.TardisItemTags;

@JeiPlugin
public class JEI implements IModPlugin{

	public static final ResourceLocation NAME = new ResourceLocation(Tardis.MODID, "jei");

	@Override
	public ResourceLocation getPluginUid() {
		return NAME;
	}
	
	@Override
	public void registerGuiHandlers(IGuiHandlerRegistration registry) {
		registry.addRecipeClickArea(QuantiscopeWeldScreen.class, 85, 30, 24, 16, WeldRecipeCategory.NAME);
		registry.addRecipeClickArea(AlembicScreen.class, 89, 30, 23, 16, AlembicRecipeCategory.NAME);
	}
	
	@Override
	public void registerCategories(IRecipeCategoryRegistration reg) {
		reg.addRecipeCategories(new AlembicRecipeCategory(reg.getJeiHelpers().getGuiHelper()));
		reg.addRecipeCategories(new ARSRecipeCategory(reg.getJeiHelpers().getGuiHelper()));
		reg.addRecipeCategories(new WeldRecipeCategory(reg.getJeiHelpers().getGuiHelper()));
		reg.addRecipeCategories(new AttunableRecipeCategory(reg.getJeiHelpers().getGuiHelper()));
	}

	@Override
	public void registerRecipes(IRecipeRegistration reg) {
		ClientWorld world = Objects.requireNonNull(Minecraft.getInstance().world);
		//Alembic
		reg.addRecipes(AlembicRecipe.getAllRecipes(world), AlembicRecipeCategory.NAME);
		
		//ARS
		List<ARSRecipe> ars = new ArrayList<ARSRecipe>();
		for(Item item : ForgeRegistries.ITEMS) {
			if(item.isIn(TardisItemTags.ARS))
				ars.add(new ARSRecipe(new ItemStack(item)));
		}
		reg.addRecipes(ars, ARSRecipeCategory.NAME);
		
		
		//Weld
		reg.addRecipes(WeldRecipe.getAllRecipes(world), WeldRecipeCategory.NAME);
		
		//Attunable
		Collection<AttunableRecipe> attunable = AttunableRecipe.getAllRecipes(world);
		for(Item item : ForgeRegistries.ITEMS) {
			//TODO: Move Tardis keys to an item tag?
			if (item instanceof IAttunable) { //Add anything that uses our base interface
				IAttunable attunableItem = (IAttunable)item;
				attunable.add(new AttunableRecipe(false, attunableItem.getAttunementTime(), Ingredient.fromItems(item), new RecipeResult(item)).setRegistryId(item.getRegistryName()));
			}
		}
		reg.addRecipes(attunable, AttunableRecipeCategory.NAME);
	}

}
