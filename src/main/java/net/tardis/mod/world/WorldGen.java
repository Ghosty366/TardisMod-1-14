package net.tardis.mod.world;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import com.google.common.collect.ImmutableList;
import com.mojang.datafixers.util.Either;
import com.mojang.datafixers.util.Pair;

import it.unimi.dsi.fastutil.objects.Object2IntLinkedOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.gen.feature.jigsaw.JigsawPattern;
import net.minecraft.world.gen.feature.jigsaw.JigsawPattern.PlacementBehaviour;
import net.minecraft.world.gen.feature.jigsaw.JigsawPiece;
import net.minecraft.world.gen.feature.structure.DesertVillagePools;
import net.minecraft.world.gen.feature.structure.PlainsVillagePools;
import net.minecraft.world.gen.feature.structure.SavannaVillagePools;
import net.minecraft.world.gen.feature.template.ProcessorLists;
import net.tardis.mixin.LegacySingleJigsawMixin;
import net.tardis.mod.Tardis;

public class WorldGen {
	
	public static List<BlockPos> BROKEN_EXTERIORS = new ArrayList<>();
	
	/**
	 * Add all Structures to existing Jigsaw Pools
	 * <br> Call in FMLCommonSetup in an enqueueWork lambda
	 */
	public static void addStructuresToJigsawPools() {
		//Initialise Jigsaw pools before we add to them
		PlainsVillagePools.init();
		SavannaVillagePools.init();
		DesertVillagePools.init();
		
		//Add Village version of Observatory Structure to following village types
		//This is using a special jigsaw compatible version of our structures
		for(String biome : new String[]{"plains", "savanna", "desert",})
			addStructureToPool(new ResourceLocation("village/"+biome+"/houses"), new ResourceLocation(Tardis.MODID, "village/" + biome + "/houses/" + biome + "_observatory"), 6);
	}
	
	/**
	 * Add a Structure to an existing Jigsaw Pool
	 * @param pool - target pool to add our structure to
	 * @param toAdd - ResourceLocation of the structure we are adding
	 * @param weight - how much our structure should be prioritised to spawn in. The higher the weight is, the less chance of spawning
	 * <br> Based off https://github.com/BluSunrize/ImmersiveEngineering/blob/1.16.5/src/main/java/blusunrize/immersiveengineering/common/world/Villages.java
	 */
	private static void addStructureToPool(ResourceLocation pool, ResourceLocation toAdd, int weight){
		JigsawPattern old = WorldGenRegistries.JIGSAW_POOL.getOrDefault(pool);

		// Fixed seed to prevent inconsistencies between different worlds
		List<JigsawPiece> shuffled;
		if(old != null)
			shuffled = old.getShuffledPieces(new Random(0));
		else
			shuffled = ImmutableList.of();
		Object2IntMap<JigsawPiece> newPieces = new Object2IntLinkedOpenHashMap<>();
		for(JigsawPiece p : shuffled)
			newPieces.computeInt(p, (JigsawPiece pTemp, Integer i) -> (i==null?0: i)+1);
		newPieces.put(LegacySingleJigsawMixin.construct(
				Either.left(toAdd), () -> ProcessorLists.EMPTY, PlacementBehaviour.RIGID
		), weight);
		List<Pair<JigsawPiece, Integer>> newPieceList = newPieces.object2IntEntrySet().stream()
				.map(e -> Pair.of(e.getKey(), e.getIntValue()))
				.collect(Collectors.toList());

		ResourceLocation name = old.getName();
		Registry.register(WorldGenRegistries.JIGSAW_POOL, pool, new JigsawPattern(pool, name, newPieceList));
	}

    @Nullable
    public static BlockPos getClosestBrokenExterior(int x, int y) {
        int dist = Integer.MAX_VALUE;
        BlockPos ext = null;
        for (BlockPos pos : BROKEN_EXTERIORS) {
            if (pos.distanceSq(new BlockPos(x, 0, y)) < dist)
                ext = pos;

        }
        if (BROKEN_EXTERIORS.contains(ext))
            BROKEN_EXTERIORS.remove(ext);
        return ext;
    }
}
