package net.tardis.mod.world.feature;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.feature.ProbabilityConfig;
import net.minecraft.world.gen.placement.ChanceConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.placement.TopSolidRangeConfig;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.config.TConfig;

public class TFeatures {
	
	public static final DeferredRegister<Feature<?>> FEATURES = DeferredRegister.create(ForgeRegistries.FEATURES, Tardis.MODID);
    
	public static final RegistryObject<Feature<OreFeatureConfig>> CINNABAR_ORE = FEATURES.register("cinnabar_ore", () -> new OreFeature(OreFeatureConfig.CODEC));
	public static final RegistryObject<Feature<ProbabilityConfig>> XION_CRYSTAL = FEATURES.register("xion_crystal", () -> new XionCrystalFeature(ProbabilityConfig.CODEC));
	public static final RegistryObject<Feature<ProbabilityConfig>> MOON_CRATOR = FEATURES.register("moon_crator", () -> new CratorFeature(ProbabilityConfig.CODEC));
    
	public static final RegistryObject<Feature<ProbabilityConfig>> BROKEN_EXTERIOR = FEATURES.register("broken_exterior", () -> new TardisGenFeature(ProbabilityConfig.CODEC));
	
	public static class ConfiguredFeatures{
		public static final ConfiguredFeature<?, ?> CONFIGURED_CINNABAR_ORE = TFeatures.CINNABAR_ORE.get().withConfiguration(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.BASE_STONE_OVERWORLD, 
				TBlocks.cinnabar_ore.get().getDefaultState(), 5)) //vein size of 5
				.withPlacement(Placement.RANGE.configure(new TopSolidRangeConfig(6, 0, 64)).chance(TConfig.COMMON.cinnabarOreSpawnChance.get())).square().count(5);
		public static final ConfiguredFeature<?, ?> CONFIGURED_XION_CRYSTAL = TFeatures.XION_CRYSTAL.get().withConfiguration(new ProbabilityConfig(TConfig.COMMON.xionCrystalSpawnChance.get())).withPlacement(Placement.RANGE.configure(new TopSolidRangeConfig(6, 5, 32))).range(16).square().count(8);

		public static final ConfiguredFeature<?, ?> CONFIGURED_MOON_CRATOR = TFeatures.MOON_CRATOR.get().withConfiguration(new ProbabilityConfig(0.1F)).withPlacement(Placement.CHANCE.configure(new ChanceConfig(14))).square();

		public static final ConfiguredFeature<?, ?> CONFIGURED_BROKEN_EXTERIOR = TFeatures.BROKEN_EXTERIOR.get().withConfiguration(new ProbabilityConfig(TConfig.COMMON.tardisSpawnProbability.get())).withPlacement(Placement.RANGE.configure(new TopSolidRangeConfig(6, 0, 64))).square();
	}
	
	/**
	 * Registers Configured Versions of the Features in FMLCommonSetup
	 * <br> Prevents mod incompatibility issues where we can accidentally delete other mod's world gen features
	 */
	public static void registerConfiguredFeatures() {
		registerConfiguredFeature("cinnabar_ore", ConfiguredFeatures.CONFIGURED_CINNABAR_ORE);
    	registerConfiguredFeature("xion_crystal", ConfiguredFeatures.CONFIGURED_XION_CRYSTAL);
    	registerConfiguredFeature("moon_crator", ConfiguredFeatures.CONFIGURED_MOON_CRATOR);
    	registerConfiguredFeature("broken_exterior", ConfiguredFeatures.CONFIGURED_BROKEN_EXTERIOR);
    }
	
	private static <T extends Feature<?>> void registerConfiguredFeature(String registryName, ConfiguredFeature<?,?> configuredFeature) {
    	Registry<ConfiguredFeature<?, ?>> registry = WorldGenRegistries.CONFIGURED_FEATURE;
    	Registry.register(registry, new ResourceLocation(Tardis.MODID, registryName), configuredFeature);
    }
}
