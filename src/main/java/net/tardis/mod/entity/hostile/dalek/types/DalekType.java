package net.tardis.mod.entity.hostile.dalek.types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.MoveThroughVillageGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.PrioritizedGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.ai.WeaponTypes;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;
import net.tardis.mod.sounds.TSounds;

public class DalekType extends ForgeRegistryEntry<DalekType> {

    SoundEvent[] ambientSounds = null;
    SoundEvent[] deathSounds = null;
    SoundEvent[] fireSounds = null;
    List<String> varients = new ArrayList<>();
    WeaponTypes.IWeapon iWeapon = null;

    public DalekType addVarient(String varient) {
        varients.add(varient);
        return this;
    }

    public DalekType addVarient(String[] varient) {
        varients.addAll(Arrays.asList(varient));
        return this;
    }

    public void setupDalek(DalekEntity dalekEntity) {
        //Tasks
        for (PrioritizedGoal goal : dalekEntity.goalSelector.goals) {
            dalekEntity.goalSelector.removeGoal(goal);
        }

        for (PrioritizedGoal goal : dalekEntity.targetSelector.goals) {
            dalekEntity.targetSelector.removeGoal(goal);
        }

        dalekEntity.goalSelector.addGoal(8, new LookAtGoal(dalekEntity, PlayerEntity.class, 8.0F));
        dalekEntity.goalSelector.addGoal(8, new LookRandomlyGoal(dalekEntity));
        dalekEntity.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(dalekEntity, LivingEntity.class, 10, true, false, getAttackPredicate(dalekEntity)));
        dalekEntity.goalSelector.addGoal(6, new MoveThroughVillageGoal(dalekEntity, 1.0D, true, 4, () -> false));
        dalekEntity.goalSelector.addGoal(7, new WaterAvoidingRandomWalkingGoal(dalekEntity, 1.0D));
        dalekEntity.targetSelector.addGoal(1, (new HurtByTargetGoal(dalekEntity)).setCallsForHelp(DalekEntity.class));
        dalekEntity.goalSelector.addGoal(1, new DalekEntity.AttackGoal(dalekEntity));
        //Attributes
        dalekEntity.getAttribute(Attributes.MAX_HEALTH).setBaseValue(20D);
        dalekEntity.getAttribute(Attributes.MOVEMENT_SPEED).setBaseValue(0.25D);
        dalekEntity.getAttribute(Attributes.ATTACK_DAMAGE).setBaseValue(5D);

    }

    public void tickSpecial(DalekEntity dalekEntity) {
    
    }

    //Sounds
    public SoundEvent getAmbientSound(DalekEntity dalekEntity) {
        if (ambientSounds == null) {
            ambientSounds = new SoundEvent[]{TSounds.DALEK_HOVER.get(), TSounds.DALEK_ARM.get()};
        }
        return ambientSounds[(int) (System.currentTimeMillis() % ambientSounds.length)];
    }

    public SoundEvent getFireSound(DalekEntity dalekEntity) {
        if (fireSounds == null) {
            fireSounds = new SoundEvent[]{TSounds.DALEK_FIRE.get(), TSounds.DALEK_EXTERMINATE.get()};
        }
        return fireSounds[(int) (System.currentTimeMillis() % fireSounds.length)];
    }

    public SoundEvent getDeathSound(DalekEntity dalekEntity) {
        if (deathSounds == null) {
            deathSounds = new SoundEvent[]{TSounds.DALEK_DEATH.get()};
        }
        return deathSounds[(int) (System.currentTimeMillis() % deathSounds.length)];
    }

    public String getRandomTexture(DalekEntity dalekEntity) {
        return varients.get(dalekEntity.world.rand.nextInt(varients.size()));
    }

    public Predicate<LivingEntity> getAttackPredicate(DalekEntity dalekEntity) {
        return livingEntity -> livingEntity.getType() != TEntities.DALEK.get();
    }

    public DalekType setWeapon(WeaponTypes.IWeapon weapon) {
        this.iWeapon = weapon;
        return this;
    }

    public WeaponTypes.IWeapon getWeapon() {
        return iWeapon;
    }


}
