package net.tardis.mod.protocols;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.tileentities.ConsoleTile;

public class ToggleAlarmProtocol extends Protocol {
	
    @Override
	public void call(World world, PlayerEntity player, ConsoleTile console) {
		if(!world.isRemote) 
			console.getInteriorManager().setAlarmOn(!console.getInteriorManager().isAlarmOn());
		else ClientHelper.openGUI(Constants.Gui.NONE, new GuiContext());
	}

	@Override
	public String getSubmenu() {
		return "security";
	}

}
