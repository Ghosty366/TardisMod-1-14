package net.tardis.mod.boti.stores;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;

import java.util.UUID;

public class EntityStorage extends AbstractEntityStorage {

    public double posX, posY, posZ;
    public float yaw, pitch, limbSwing = 0, limbSwingAmount= 0;
    public ResourceLocation type;
    public UUID id;
    public CompoundNBT data;

    Entity entity;

    public EntityStorage(Entity e) {
        posX = e.getPosX();
        posY = e.getPosY();
        posZ = e.getPosZ();

        yaw = e.rotationYaw;
        pitch = e.rotationPitch;

        if(e instanceof LivingEntity){
            LivingEntity livingEntity = (LivingEntity) e;
            limbSwing = livingEntity.limbSwing;
            limbSwingAmount = livingEntity.limbSwingAmount;
        }

        id = e.getUniqueID();
        type = e.getType().getRegistryName();
        data = e.serializeNBT();
    }

    public EntityStorage(PacketBuffer buf) {
        this.decode(buf);
    }

    @Override
    public void encode(PacketBuffer buf) {
        buf.writeDouble(posX);
        buf.writeDouble(posY);
        buf.writeDouble(posZ);

        buf.writeFloat(yaw);
        buf.writeFloat(pitch);
        buf.writeFloat(limbSwing);
        buf.writeFloat(limbSwingAmount);

        buf.writeUniqueId(id);
        buf.writeResourceLocation(type);
        buf.writeCompoundTag(data);

    }

    @Override
    public void decode(PacketBuffer buf) {
        posX = buf.readDouble();
        posY = buf.readDouble();
        posZ = buf.readDouble();

        yaw = buf.readFloat();
        pitch = buf.readFloat();
        limbSwing = buf.readFloat();
        limbSwingAmount = buf.readFloat();

        id = buf.readUniqueId();
        type = buf.readResourceLocation();
        data = buf.readCompoundTag();
    }

    public void updateEntity(Entity e) {

        e.lastTickPosX = e.prevPosX = e.getPosX();
        e.lastTickPosY = e.prevPosY = e.getPosY();
        e.lastTickPosZ = e.prevPosZ = e.getPosZ();

        e.setPosition(posX, posY, posZ);

        e.prevRotationYaw = this.yaw;
        e.prevRotationPitch = this.pitch;
        e.rotationYaw = this.yaw;
        e.rotationPitch = this.pitch;

        if(e instanceof LivingEntity){
            LivingEntity livingEntity = (LivingEntity) e;
            livingEntity.limbSwing = this.limbSwing;
            livingEntity.limbSwingAmount = this.limbSwingAmount;
        }
    }
}
