package net.tardis.mod.boti;

import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.LightType;
import net.minecraft.world.World;

/** Helper class that runs client side only code, prevents classloading, ClassNotFound or NoSuchMethod errors on dedicated servers*/
public class BotiClientHelper {
    
    public static int packLight(int blockLightIn, int skyLightIn) {
        return blockLightIn << 4 | skyLightIn << 20;
     }

}
